<?php
include "AuxDB.php";
?>

<html>
<head>
	<title>Rutas</title>
	<link href="css/styles.css" rel="stylesheet" type="text/css" />
	<script src="js/js.js"></script>
</head>
<body>
	<?php
	//Establecemos conexión con la BD
	$db = new AuxDB();
	$db->conectar();
	//Ejecutamos la consulta SQL
	$sql = "SELECT * FROM Rutas";
	$result = $db->ejecutarSQL($sql);
	?>

	<div class="titulo"><a class="titulo" href="index.html">iBarco</a></div>
	<br><br>
	<input type="button" onClick="javascript:editar('rutanueva.php');" value="Nueva" name="Nueva"/>
	<fieldset>
		<legend class="titulo">Rutas ( <?php echo $db->cantidadFilas($result); ?> )</legend> 
		<TABLE id="tabladatos" BORDER=0 CELLSPACING=0 CELLPADDING=0>
			<tr>
				<td class="tdid">ID</td><td class="tdnombre">Nombre</td><td class="tdprovincia">Descripcion</td><td class="tdlatitud">Distancia</td><td class="tdver"><br></td></tr>

				<?php  
//Recorremos las filas devueltas por la consulta
				while($row = $db->siguienteFila($result) ){	
					echo("<tr onmouseover='this.style.background=\"#DDDEDE\"' onmouseout='this.style.background=\"#FFFFFF\"'><td class=\"tddatosid\">" . $row["id"] .  "</td>");
					echo("<td class=\"tddatosnombre\">" . $row["nombre"] . "</td>");
					echo("<td class=\"tddatosprovincia\">" . $row["descripcion"] . "</td>");
					echo("<td class=\"tddatosprovincia\">" . $row["distancia"] . "</td>");
					echo("<td> <input type=\"button\" onClick=\"javascript:editar('ruta.php?id="  . $row["id"] . "');\" value=\"Ver\" name=\"Ver\"/></td></tr>");

				}
				?>
			</TABLE>
		</fieldset>
	</body>
	</html> 