<?php
include 'AuxDB.php';
include 'clUsuario.php';

$db = new AuxDB();
$db->conectar();

$sql = "SELECT * FROM Puntos";
$rst = $db->ejecutarSQL($sql);
$NumFilas = $db->cantidadFilas($rst);
$fila = $db->siguienteFila($rst);

?>
<html>
<head>
	<title>Tablas PUNTOS DE INTERÉS</title>
	<link href="css/styles.css" rel="stylesheet" type="text/css" />
	<script src="js/js.js"></script>
</head>
<body>
Tabla PUNTOS DE INTERÉS (Tabla Puntos)
	<table border='1' id='tabladatosPuntos' >
		<tr>
			<th>Id</th>
			<th>Nombre</th>
			<th>Longitud</th>
			<th>Latitud</th>
			<th>Long.Grados</th>
			<th>Long.Minutos</th>
			<th>Long.Segundos</th>
			<th>Lat.Grados</th>
			<th>Lat.Minutos</th>
			<th>Lat.Segundos</th>
			<th>Descripción</th>
			<th>Fotos</th>
		</tr>

		<?php for ($nf = 1; $nf <= $NumFilas; $nf++) { ?>

			<tr>
				<td class = 'tdUsuarios'><?php printf($fila['id']);?></td>
				<td class = 'tdUsuarios'><?php printf($fila['Nombre']);?></td>
				<td class = 'tdUsuarios'><?php printf($fila['Longitud']);?></td>
				<td class = 'tdUsuarios'><?php printf($fila['Latitud']);?></td>
				<td class = 'tdUsuarios'><?php printf($fila['longitudGrados']);?></td>
				<td class = 'tdUsuarios'><?php printf($fila['longitudMinutos']);?></td>
				<td class = 'tdUsuarios'><?php printf($fila['longitudSegundos']);?></td>
				<td class = 'tdUsuarios'><?php printf($fila['latitudGrados']);?></td>
				<td class = 'tdUsuarios'><?php printf($fila['latitudMinutos']);?></td>
				<td class = 'tdUsuarios'><?php printf($fila['latitudSegundos']);?></td>
				<td class = 'tdUsuarios'><?php printf($fila['Descripcion']);?></td>
				<td class = 'tdUsuarios'><?php printf($fila['Fotos']);?></td>
				<td>
				<form method='get' action='valorarPuntoInteres.php'>
					<input type='hidden' name='idPunto' value='<?php printf($fila['id']);?>' />
					<input type="submit" name="valorar" value="valorar" />
				</form>
				</td>
			</tr>	
			<?php $fila = $db->siguienteFila($rst);
		 } ?>


	</table>
	<br />
<?php
	$db->desconectar();
?>
<a class = 'tdUsuarios' href='indexPuntos.php'>Volver</a>
</body>
</html>