<?php
include "clPuerto.php";
//Ejecutara la accion de modificar

if (isset($_POST['enviar'])) {
	// creamos un nuevo puerto
	$oPuerto = new Puerto($_POST["id"],$_POST["nombre"],$_POST["latitud"],$_POST["longitud"],$_POST["direccion"],$_POST["poblacion"],$_POST["cp"],$_POST["provincia"],$_POST["telefono"],$_POST["fax"],$_POST["www"],$_POST["email"],$_POST["caladomin"],$_POST["esloramax"],$_POST["namarres"],$_POST["canalvhf"],$_POST["servagua"],$_POST["servelectricidad"],$_POST["servgasolina"],$_POST["servgrua"],$_POST["servtravellift"],$_POST["servrampa"],$_POST["servtaller"],$_POST["servmuelleespera"],$_POST["servbar"],$_POST["servrestaurante"],$_POST["servsupermercado"],$_POST["servhielo"],$_POST["servrecogidaaceite"],$_POST["servrecogidabasura"],$_POST["servaguasnegras"],$_POST["servfarmacia"],$_POST["servmedico"],$_POST["servmeteo"],$_POST["servbanco"],$_POST["servalquilercoche"],$_POST["otros"],$_POST["descripcion"]);

}

?>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Puerto</title>
	<link href="css/styles.css" rel="stylesheet" type="text/css" />
	<script src="js/js.js"></script>
</head>
<body>
	<div class="titulo"><a class="titulo" href="index.html">iBarco</a></div>
	<br><br>
	<form method="post" id="formPuerto" action="puertomodificar.php">
		<fieldset>
			<legend class="titulo">Editando datos de Puerto</legend>   
			<div>
				<label for="nombre">Nombre</label>
				<input type="text" class="grande" id="nombre" name="nombre" value="<?php echo $oPuerto->getPuertoNombre(); ?>" />
			</div>
			<br />
			<div class="camposjuntos">
				<label for="latitud">Latitud</label>
				<input type="text" class="corto" id="latitud" name="latitud" value="<?php echo $oPuerto->getPuertoLatitud(); ?>"  />
			</div>

			<div class="camposjuntos">
				<label for="longitud">Longitud</label>
				<input type="text" class="corto" id="longitud" name="longitud" value="<?php echo $oPuerto->getPuertoLongitud(); ?>"  />
			</div>
			<br />
			<div class="contenedor">
				<br />
			</div>	
			<div>
				<label for="direccion">Direccion</label>
				<input type="text" class="corto" id="direccion" name="direccion" value="<?php echo $oPuerto->getPuertoDireccion();  ?>"  />
			</div>
			<br />
			<div>
				<label for="poblacion">Poblacion</label>
				<input type="text" class="corto" id="poblacion" name="poblacion" value="<?php echo $oPuerto->getPuertoPoblacion(); ?>"  />
			</div>
			<br />
			<div>
				<label for="cp">CP</label>
				<input type="text" class="corto" id="cp" name="cp" value="<?php echo $oPuerto->getPuertoCP(); ?>"  />
			</div>
			<br />
			<div>
				<label for="provincia">Provincia</label>
				<input type="text" class="corto" id="provincia" name="provincia" value="<?php echo $oPuerto->getPuertoProvincia(); ?>"  />
			</div>
			<br />
			<div>
				<label for="telefono">Telefono</label>
				<input type="text" class="corto" id="telefono" name="telefono" value="<?php echo $oPuerto->getPuertoTelefono(); ?>"  />
			</div>
			<br />
			<div>
				<label for="fax">Fax</label>
				<input type="text" class="corto" id="fax" name="fax" value="<?php echo $oPuerto->getPuertoFax(); ?>"  />
			</div>
			<br />
			<div>
				<label for="www">Www</label>
				<input type="text" class="corto" id="www" name="www" value="<?php echo $oPuerto->getPuertoWww();  ?>"  />
			</div>
			<br />
			<div>
				<label for="email">Email</label>
				<input type="text" class="corto" id="email" name="email" value="<?php echo $oPuerto->getPuertoEmail(); ?>"  />
			</div>
			<br />
			<div>
				<label for="caladomin">Calado minimo</label>
				<input type="text" class="corto" id="caladomin" name="caladomin" value="<?php echo $oPuerto->getPuertoCaladoMin();  ?>"  />
			</div>
			<br />
			<div>
				<label for="esloramax">Eslora maxima</label>
				<input type="text" class="corto" id="esloramax" name="esloramax" value="<?php echo $oPuerto->getPuertoEslora();  ?>"  />
			</div>
			<br />
			<div>
				<label for="namarres">N de amarres</label>
				<input type="text" class="corto" id="namarres" name="namarres" value="<?php echo $oPuerto->getPuertoNAmarres();  ?>"  />
			</div>
			<br />
			<div>
				<label for="canalvhf">Canal vhf</label>
				<input type="text" class="corto" id="canalvhf" name="canalvhf" value="<?php echo $oPuerto->getPuertoCanalVHF(); ?>"  />
			</div>
			<br />
			<div class="camposjuntos">
				<label for="servagua">Serv. Agua</label>
				<input type="checkbox" name="servagua" value="1"
				<?php if($oPuerto->getPuertoServ_Agua() == "1"){
					echo("CHECKED");
				}?>
				/>
			</div>
			
			<div class="camposjuntos">
				<label for="servelectricidad">Serv. Electricidad</label>
				<input type="checkbox" class="corto" id="servelectricidad" name="servelectricidad" value="1"
				<?php if($oPuerto->getPuertoServ_Electricidad() == "1"){
					echo("CHECKED");
				}?>
				/>
			</div>
			
			<div class="camposjuntos">
				<label for="servgasolina">Serv. Gasolina</label>
				<input type="checkbox" class="corto" id="servgasolina" name="servgasolina" value="1"
				<?php if($oPuerto->getPuertoServ_Gasolina() == "1"){
					echo("CHECKED");
				}?>
				/>
			</div>
			
			<div class="camposjuntos">
				<label for="servgrua">Serv. Grua</label>
				<input type="checkbox" class="corto" id="servgrua" name="servgrua" value="1"
				<?php if($oPuerto->getPuertoServ_Grua() == "1"){
					echo("CHECKED");
				}?>
				/>
			</div>
			
			<div class="camposjuntos">
				<label for="servtravellift">Serv. travellift</label>
				<input type="checkbox" class="corto" id="servtravellift" name="servtravellift" value="1"
				<?php if($oPuerto->getPuertoServ_Travellift() == "1"){
					echo("CHECKED");
				}?>
				/>
			</div>
			
			<div class="camposjuntos">
				<label for="servrampa">Serv. rampa</label>
				<input type="checkbox" class="corto" id="servrampa" name="servrampa" value="1"
				<?php if($oPuerto->getPuertoServ_Rampa() == "1"){
					echo("CHECKED");
				}?>
				/>
			</div>
			
			<div class="camposjuntos">
				<label for="servtaller">Serv. taller</label>
				<input type="checkbox" class="corto" id="servtaller" name="servtaller" value="1"
				<?php if($oPuerto->getPuertoServ_Taller() == "1"){
					echo("CHECKED");
				}?>
				/>
			</div>
			
			<div class="camposjuntos">
				<label for="servmuelleespera">Serv. muelle de espera</label>
				<input type="checkbox" class="corto" id="servmuelleespera" name="servmuelleespera" value="1"
				<?php if($oPuerto->getPuertoServ_MuelleEspera() == "1"){
					echo("CHECKED");
				}?>
				/>
			</div>
			
			<div class="camposjuntos">
				<label for="servbar">Serv. Bar</label>
				<input type="checkbox" class="corto" id="servbar" name="servbar" value="1"
				<?php if($oPuerto->getPuertoServ_Bar() == "1"){
					echo("CHECKED");
				}?>
				/>
			</div>
			
			<div class="camposjuntos">
				<label for="servrestaurante">Serv. restaurante</label>
				<input type="checkbox" class="corto" id="servrestaurante" name="servrestaurante" value="1"
				<?php if($oPuerto->getPuertoServ_Restaurante() == "1"){
					echo("CHECKED");
				}?>
				/>
			</div>
			
			<div class="camposjuntos">
				<label for="servsupermercado">Serv. supermercado</label>
				<input type="checkbox" class="corto" id="servsupermercado" name="servsupermercado" value="1"
				<?php if($oPuerto->getPuertoServ_Supermercado() == "1"){
					echo("CHECKED");
				}?>
				/>
			</div>
			
			<div class="camposjuntos">
				<label for="servhielo">Serv. hielo</label>
				<input type="checkbox" class="corto" id="servhielo" name="servhielo" value="1"
				<?php if($oPuerto->getPuertoServ_Hielo() == "1"){
					echo("CHECKED");
				}?>
				/>
			</div>
			
			<div class="camposjuntos">
				<label for="servrecogidaaceite">Serv. recogida aceite</label>
				<input type="checkbox" class="corto" id="servrecogidaaceite" name="servrecogidaaceite" value="1"
				<?php if($oPuerto->getPuertoServ_RecogidaAceite() == "1"){
					echo("CHECKED");
				}?>
				/>
			</div>
			
			<div class="camposjuntos">
				<label for="servrecogidabasura">Serv. recogida basura</label>
				<input type="checkbox" class="corto" id="servrecogidabasura" name="servrecogidabasura" value="1"
				<?php if($oPuerto->getPuertoServ_RecogidaBasura() == "1"){
					echo("CHECKED");
				}?>
				/>
			</div>
			
			<div class="camposjuntos">
				<label for="servaguasnegras">Serv. aguas negras</label>
				<input type="checkbox" class="corto" id="servaguasnegras" name="servaguasnegras" value="1"
				<?php if($oPuerto->getPuertoServ_AguasNegras() == "1"){
					echo("CHECKED");
				}?>
				/>
			</div>
			
			<div class="camposjuntos">
				<label for="servfarmacia">Serv. farmacia</label>
				<input type="checkbox" class="corto" id="servfarmacia" name="servfarmacia" value="1"
				<?php if($oPuerto->getPuertoServ_Farmacia() == "1"){
					echo("CHECKED");
				}?>
				/>
			</div>
			
			<div class="camposjuntos">
				<label for="servmedico">Serv. medico</label>
				<input type="checkbox" class="corto" id="servmedico" name="servmedico" value="1"
				<?php if($oPuerto->getPuertoServ_Medico() == "1"){
					echo("CHECKED");
				}?>
				/>
			</div>
			
			<div class="camposjuntos">
				<label for="servmeteo">Serv. meteo</label>
				<input type="checkbox" class="corto" id="servmeteo" name="servmeteo" value="1"
				<?php if($oPuerto->getPuertoServ_Meteo() == "1"){
					echo("CHECKED");
				}?>
				/>
			</div>
			
			<div class="camposjuntos">
				<label for="servbanco">Serv. banco</label>
				<input type="checkbox" class="corto" id="servbanco" name="servbanco" value="1"
				<?php if($oPuerto->getPuertoServ_Banco() == "1"){
					echo("CHECKED");
				}?>
				/>
			</div>
			
			<div class="camposjuntos">
				<label for="servalquilercoche">Serv. alquiler coche</label>
				<input type="checkbox" class="corto" id="servalquilercoche" name="servalquilercoche" value="1"
				<?php if($oPuerto->getPuertoServ_AlquilerCoche() == "1"){
					echo("CHECKED");
				}?>
				/>
			</div>
			<br /><br />
			<div class="contenedor">
				<br />
			</div>	
			<div>
				<label for="otros">Otros</label>
				<input type="text" class="grande" id="otros" name="otros" value="<?php echo $oPuerto->getPuertoOtros();  ?>"  />
			</div>
			<br />
			<div>
				<label for="descripcion">Descripcion</label>
				<input type="text" class="grande" id="descripcion" name="descripcion" value="<?php echo $oPuerto->getPuertoDescripcion(); ?>"  />
			</div>
			<br />

			<div>
				<input type="hidden" id="id" name="id" value="<?php echo $oPuerto->getPuertoID(); ?>" />
				<input type="submit" class="btn" value="Guardar" name="enviar"/>
				<input type="button" class="btn" value="Limpiar" name="limpiar"/>
				<input type="button" onClick="javascript:history.back();" value="Cancelar" name="Cancelar"/>
				<input type="submit" class="btn" value="Eliminar" name="eliminar"/>
			</div>
		</fieldset>
	</form>
</body>
</html>