<?php

require_once "AuxDB.php";

// clase Puerto

class Puerto {
	
	private $puerto_ID;
	private $puerto_Nombre;
	private $puerto_Latitud;
	private $puerto_Longitud;
	private $puerto_Direccion;
	private $puerto_Poblacion;
	private $puerto_CP;
	private $puerto_Provincia;
	private $puerto_Telefono;
	private $puerto_Fax;
	private $puerto_Www;
	private $puerto_Email;
	private $puerto_Calado_Min;
	private $puerto_Eslora_Max;
	private $puerto_Namarres;
	private $puerto_CanalVhf;
	private $puerto_Serv_Agua;
	private $puerto_Serv_Electricidad;
	private $puerto_Serv_Gasolina;
	private $puerto_Serv_Grua;
	private $puerto_Serv_Travellift;
	private $puerto_Serv_Rampa;
	private $puerto_Serv_Taller;
	private $puerto_Serv_MuelleEspera;
	private $puerto_Serv_Bar;
	private $puerto_Serv_Restaurante;
	private $puerto_Serv_Supermercado;
	private $puerto_Serv_Hielo;
	private $puerto_Serv_RecogidaAceite;
	private $puerto_Serv_RecogidaBasura;
	private $puerto_Serv_AguasNegras;
	private $puerto_Serv_Farmacia;
	private $puerto_Serv_Medico;
	private $puerto_Serv_Meteo;
	private $puerto_Serv_Banco;
	private $puerto_Serv_AlquilerCoche;
	private $puerto_Otros;
	private $puerto_Descripcion;

	
	// Funciones de la clase Puerto
	function getPuertoID() {
		return $this->puerto_ID;
	}
	function getPuertoNombre(){
		return $this->puerto_Nombre;
	}
	function getPuertoLatitud() {
		return $this->puerto_Latitud;
	}
	function getPuertoLongitud() {
		return $this->puerto_Longitud;
	}
	function getPuertoDireccion() {
		return $this->puerto_Direccion;
	}
	function getPuertoPoblacion() {
		return $this->puerto_Poblacion;
	}
	function getPuertoCP() {
		return $this->puerto_CP;
	}
	function getPuertoProvincia() {
		return $this->puerto_Provincia;
	}
	function getPuertoTelefono() {
		return $this->puerto_Telefono;
	}
	function getPuertoFax() {
		return $this->puerto_Fax;
	}
	function getPuertoWww() {
		return $this->puerto_Www;
	}
	function getPuertoEmail() {
		return $this->puerto_Email;
	}
	function getPuertoCaladoMin() {
		return $this->puerto_Calado_Min;
	}
	function getPuertoEslora() {
		return $this->puerto_Eslora_Max;
	}
	function getPuertoNAmarres() {
		return $this->puerto_Namarres;
	}
	function getPuertoCanalVHF() {
		return $this->puerto_CanalVhf;
	}
	function getPuertoServ_Agua() {
		return $this->puerto_Serv_Agua;
	}
	function getPuertoServ_Electricidad() {
		return $this->puerto_Serv_Electricidad;
	}
	function getPuertoServ_Gasolina() {
		return $this->puerto_Serv_Gasolina;
	}
	function getPuertoServ_Grua() {
		return $this->puerto_Serv_Grua;
	}
	function getPuertoServ_Travellift() {
		return $this->puerto_Serv_Travellift;
	}
	function getPuertoServ_Rampa() {
		return $this->puerto_Serv_Rampa;
	}
	function getPuertoServ_Taller() {
		return $this->puerto_Serv_Taller;
	}
	function getPuertoServ_MuelleEspera() {
		return $this->puerto_Serv_MuelleEspera;
	}
	function getPuertoServ_Bar() {
		return $this->puerto_Serv_Bar;
	}
	function getPuertoServ_Restaurante() {
		return $this->puerto_Serv_Restaurante;
	}
	function getPuertoServ_Supermercado() {
		return $this->puerto_Serv_Supermercado;
	}
	function getPuertoServ_Hielo() {
		return $this->puerto_Serv_Hielo;
	}
	function getPuertoServ_RecogidaAceite() {
		return $this->puerto_Serv_RecogidaAceite;
	}
	function getPuertoServ_RecogidaBasura() {
		return $this->puerto_Serv_RecogidaBasura;
	}
	function getPuertoServ_AguasNegras() {
		return $this->puerto_Serv_AguasNegras;
	}
	function getPuertoServ_Farmacia() {
		return $this->puerto_Serv_Farmacia;
	}
	function getPuertoServ_Medico() {
		return $this->puerto_Serv_Medico;
	}
	function getPuertoServ_Meteo() {
		return $this->puerto_Serv_Meteo;
	}
	function getPuertoServ_Banco() {
		return $this->puerto_Serv_Banco;
	}
	function getPuertoServ_AlquilerCoche() {
		return $this->puerto_Serv_AlquilerCoche;
	}
	function getPuertoOtros() {
		return $this->puerto_Otros;
	}
	function getPuertoDescripcion() {
		return $this->puerto_Descripcion;
	}





	//Constructor objeto Puerto
	function __construct($puertoID, $puertoNombre, $puertoLatitud, $puertoLongitud, $puertoDireccion, $puertoPoblacion, $puertoCP, $puertoProvincia, $puertoTelefono, $puertoFax, $puertoWww, $puertoEmail, $puertoCalado_Min, $puertoEslora_Max, $puertoNamarres, $puertoCanalVhf, $puertoServ_Agua, $puertoServ_Electricidad, $puertoServ_Gasolina, $puertoServ_Grua, $puertoServ_Travellift, $puertoServ_Rampa, $puertoServ_Taller, $puertoServ_MuelleEspera, $puertoServ_Bar, $puertoServ_Restaurante, $puertoServ_Supermercado, $puertoServ_Hielo, $puertoServ_RecogidaAceite, $puertoServ_RecogidaBasura, $puertoServ_AguasNegras, $puertoServ_Farmacia, $puertoServ_Medico, $puertoServ_Meteo, $puertoServ_Banco, $puertoServ_AlquilerCoche, $puertoOtros, $puertoDescripcion){

		$this->puerto_ID = $puertoID;
		$this->puerto_Nombre = $puertoNombre;
		$this->puerto_Latitud = $puertoLatitud;
		$this->puerto_Longitud = $puertoLongitud;
		$this->puerto_Direccion = $puertoDireccion;
		$this->puerto_Poblacion = $puertoPoblacion;
		$this->puerto_CP = $puertoCP;
		$this->puerto_Provincia = $puertoProvincia;
		$this->puerto_Telefono = $puertoTelefono;
		$this->puerto_Fax = $puertoFax;
		$this->puerto_Www = $puertoWww;
		$this->puerto_Email = $puertoEmail;
		$this->puerto_Calado_Min = $puertoCalado_Min;
		$this->puerto_Eslora_Max = $puertoEslora_Max;
		$this->puerto_Namarres = $puertoNamarres;
		$this->puerto_CanalVhf = $puertoCanalVhf;
		$this->puerto_Serv_Agua = $puertoServ_Agua;
		$this->puerto_Serv_Electricidad = $puertoServ_Electricidad;
		$this->puerto_Serv_Gasolina = $puertoServ_Gasolina;
		$this->puerto_Serv_Grua = $puertoServ_Grua;
		$this->puerto_Serv_Travellift = $puertoServ_Travellift;
		$this->puerto_Serv_Rampa = $puertoServ_Rampa;
		$this->puerto_Serv_Taller = $puertoServ_Taller;
		$this->puerto_Serv_MuelleEspera = $puertoServ_MuelleEspera;
		$this->puerto_Serv_Bar = $puertoServ_Bar;
		$this->puerto_Serv_Restaurante = $puertoServ_Restaurante;
		$this->puerto_Serv_Supermercado = $puertoServ_Supermercado;
		$this->puerto_Serv_Hielo = $puertoServ_Hielo;
		$this->puerto_Serv_RecogidaAceite = $puertoServ_RecogidaAceite;
		$this->puerto_Serv_RecogidaBasura = $puertoServ_RecogidaBasura;
		$this->puerto_Serv_AguasNegras = $puertoServ_AguasNegras;
		$this->puerto_Serv_Farmacia = $puertoServ_Farmacia;
		$this->puerto_Serv_Medico = $puertoServ_Medico;
		$this->puerto_Serv_Meteo = $puertoServ_Meteo;
		$this->puerto_Serv_Banco = $puertoServ_Banco;
		$this->puerto_Serv_AlquilerCoche = $puertoServ_AlquilerCoche;
		$this->puerto_Otros = $puertoOtros;
		$this->puerto_Descripcion = $puertoDescripcion;

	}


	function leerPuertoNombre($PuertoNombre) {
		$db = new AuxDB();
		$db->conectar();
		$sql = "SELECT * FROM Puertos WHERE nombre = '" . $PuertoNombre . "'";

		$rst = $db->ejecutarSQL($sql);
		$db->desconectar();

		$fila = $db->siguienteFila($rst);
		
		$this->puerto_Id = $fila['id'];
		$this->puerto_Nombre = $PuertoNombre;
		$this->puerto_Latitud = $fila['latitud'];
		$this->puerto_Longitud = $fila['longitud'];
		$this->puerto_Direccion = $fila['dir_direccion'];
		$this->puerto_Poblacion = $fila['dir_poblacion'];
		$this->puerto_CP = $fila['dir_codigopostal'];
		$this->puerto_Provincia = $fila['dir_provincia'];
		$this->puerto_Telefono = $fila['telefono'];
		$this->puerto_Fax = $fila['fax'];
		$this->puerto_Www = $fila['www'];
		$this->puerto_Email = $fila['email'];
		$this->puerto_Calado_Min = $fila['calado_min'];
		$this->puerto_Eslora_Max = $fila['eslora_max'];
		$this->puerto_Namarres = $fila['namarres'];
		$this->puerto_CanalVhf = $fila['canalVhf'];
		$this->puerto_Serv_Agua = $fila['serv_agua'];
		$this->puerto_Serv_Electricidad = $fila['serv_electricidad'];
		$this->puerto_Serv_Gasolina = $fila['serv_gasolina'];
		$this->puerto_Serv_Grua = $fila['serv_grua'];
		$this->puerto_Serv_Travellift = $fila['serv_Travellift'];
		$this->puerto_Serv_Rampa = $fila['serv_rampa'];
		$this->puerto_Serv_Taller = $fila['serv_taller'];
		$this->puerto_Serv_MuelleEspera = $fila['serv_muelleEspera'];
		$this->puerto_Serv_Bar = $fila['serv_bar'];
		$this->puerto_Serv_Restaurante = $fila['serv_restaurante'];
		$this->puerto_Serv_Supermercado = $fila['serv_supermercado'];
		$this->puerto_Serv_Hielo = $fila['serv_hielo'];
		$this->puerto_Serv_RecogidaAceite = $fila['serv_recogidaaceite'];
		$this->puerto_Serv_RecogidaBasura = $fila['serv_recogidabasura'];
		$this->puerto_Serv_AguasNegras = $fila['serv_aguasnegras'];
		$this->puerto_Serv_Farmacia = $fila['serv_farmacia'];
		$this->puerto_Serv_Medico = $fila['serv_medico'];
		$this->puerto_Serv_Meteo = $fila['serv_meteo'];
		$this->puerto_Serv_Banco = $fila['serv_banco'];
		$this->puerto_Serv_AlquilerCoche = $fila['serv_alquilercoche'];
		$this->puerto_Otros = $fila['otros'];
		$this->puerto_Descripcion = $fila['descripcion'];
	}
	

	
	function leerPuertoID($PuertoID) {
		$db = new AuxDB();
		$db->conectar();
		$sql = "SELECT * FROM Puertos WHERE id = '" . $PuertoID . "'";

		$rst = $db->ejecutarSQL($sql);
		$db->desconectar();

		$fila = $db->siguienteFila($rst);
		
		$this->$puerto_Id = $PuertoID;
		$this->$puerto_Nombre = $fila['nombre'];
		$this->$puerto_Latitud = $fila['latitud'];
		$this->$puerto_Longitud = $fila['longitud'];
		$this->$puerto_Direccion = $fila['dir_direccion'];
		$this->$puerto_Poblacion = $fila['dir_poblacion'];
		$this->$puerto_CP = $fila['dir_codigopostal'];
		$this->$puerto_Provincia = $fila['dir_provincia'];
		$this->$puerto_Telefono = $fila['telefono'];
		$this->$puerto_Fax = $fila['fax'];
		$this->$puerto_Www = $fila['www'];
		$this->$puerto_Email = $fila['email'];
		$this->$puerto_Calado_Min = $fila['calado_min'];
		$this->$puerto_Eslora_Max = $fila['eslora_max'];
		$this->$puerto_Namarres = $fila['namarres'];
		$this->$puerto_CanalVhf = $fila['canalVhf'];
		$this->$puerto_Serv_Agua = $fila['serv_agua'];
		$this->$puerto_Serv_Electricidad = $fila['serv_electricidad'];
		$this->$puerto_Serv_Gasolina = $fila['serv_gasolina'];
		$this->$puerto_Serv_Grua = $fila['serv_grua'];
		$this->$puerto_Serv_Travellift = $fila['serv_Travellift'];
		$this->$puerto_Serv_Rampa = $fila['serv_rampa'];
		$this->$puerto_Serv_Taller = $fila['serv_taller'];
		$this->$puerto_Serv_MuelleEspera = $fila['serv_muelleEspera'];
		$this->$puerto_Serv_Bar = $fila['serv_bar'];
		$this->$puerto_Serv_Restaurante = $fila['serv_restaurante'];
		$this->$puerto_Serv_Supermercado = $fila['serv_supermercado'];
		$this->$puerto_Serv_Hielo = $fila['serv_hielo'];
		$this->$puerto_Serv_RecogidaAceite = $fila['serv_recogidaaceite'];
		$this->$puerto_Serv_RecogidaBasura = $fila['serv_recogidabasura'];
		$this->$puerto_Serv_AguasNegras = $fila['serv_aguasnegras'];
		$this->$puerto_Serv_Farmacia = $fila['serv_farmacia'];
		$this->$puerto_Serv_Medico = $fila['serv_medico'];
		$this->$puerto_Serv_Meteo = $fila['serv_meteo'];
		$this->$puerto_Serv_Banco = $fila['serv_banco'];
		$this->$puerto_Serv_AlquilerCoche = $fila['serv_alquilercoche'];
		$this->$puerto_Otros = $fila['otros'];
		$this->$puerto_Descripcion = $fila['descripcion'];
	}
	

	
	// Inserta nuevo Puerto
	function insertarPuerto() {

		$db=new AuxDB();
		$db->conectar();
		$sql = "INSERT INTO Puertos(nombre, latitud, longitud, dir_direccion, dir_poblacion, dir_codigopostal, dir_provincia, telefono, fax, www, email, calado_min, eslora_max, namarres, canalvhf, serv_agua, serv_electricidad, serv_gasolina, serv_grua, serv_travellift, serv_rampa, serv_taller, serv_muelleespera, serv_bar, serv_restaurante, serv_supermercado, serv_hielo, serv_recogidaaceite, serv_recogidabasura, serv_aguasnegras, serv_farmacia, serv_medico, serv_meteo, serv_banco, serv_alquilercoche, otros, descripcion) ";
		$sql.= "VALUES ('". mysql_escape_string($this->puerto_Nombre) . "', '". mysql_escape_string($this->puerto_Latitud) . "', '".mysql_escape_string($this->puerto_Longitud). "', '".mysql_escape_string($this->puerto_Direccion). "', '".mysql_escape_string($this->puerto_Poblacion). "', '".mysql_escape_string($this->puerto_CP). "', '".mysql_escape_string($this->puerto_Provincia). "', '".mysql_escape_string($this->puerto_Telefono). "', '".mysql_escape_string($this->puerto_Fax). "', '".mysql_escape_string($this->puerto_Www). "', '".mysql_escape_string($this->puerto_Email). "', '".mysql_escape_string($this->puerto_Calado_Min). "', '".mysql_escape_string($this->puerto_Eslora_Max). "', '".mysql_escape_string($this->puerto_Namarres). "', '".mysql_escape_string($this->puerto_CanalVhf). "', '".mysql_escape_string($this->puerto_Serv_Agua). "', '".mysql_escape_string($this->puerto_Serv_Electricidad). "', '".mysql_escape_string($this->puerto_Serv_Gasolina). "', '".mysql_escape_string($this->puerto_Serv_Grua). "', '".mysql_escape_string($this->puerto_Serv_Travellift). "', '".mysql_escape_string($this->puerto_Serv_Rampa). "', '".mysql_escape_string($this->puerto_Serv_Taller). "', '".mysql_escape_string($this->puerto_Serv_MuelleEspera). "', '".mysql_escape_string($this->puerto_Serv_Bar). "', '".mysql_escape_string($this->puerto_Serv_Restaurante). "', '".mysql_escape_string($this->puerto_Serv_Supermercado). "', '".mysql_escape_string($this->puerto_Serv_Hielo). "', '".mysql_escape_string($this->puerto_Serv_RecogidaAceite). "', '".mysql_escape_string($this->puerto_Serv_RecogidaBasura). "', '".mysql_escape_string($this->puerto_Serv_AguasNegras). "', '".mysql_escape_string($this->puerto_Serv_Farmacia). "', '".mysql_escape_string($this->puerto_Serv_Medico). "', '".mysql_escape_string($this->puerto_Serv_Meteo). "', '".mysql_escape_string($this->puerto_Serv_Banco). "', '".mysql_escape_string($this->puerto_Serv_AlquilerCoche). "', '".mysql_escape_string($this->puerto_Otros). "', '".mysql_escape_string($this->puerto_Descripcion). "')";


		$db->ejecutarSQL($sql);
		//$ret=0;
		//if ($db) { //insercion correcta
			//$ret=1;
		//}
		$db->desconectar();
		return $ret;
	} 
	

	
	//Modificar Puerto
	function modificaPuerto() {
		$db = new AuxDB();
		$db->conectar();

		$pID = $this->puerto_ID;

		$puertoNombreNuevo = $this->puerto_Nombre;
		$puertoLatitudNuevo = $this->puerto_Latitud;
		$puertoLongitudNuevo = $this->puerto_Longitud;
		$puertoDireccionNuevo = $this->puerto_Direccion;
		$puertoPoblacionNuevo = $this->puerto_Poblacion;
		$puertoCPNuevo = $this->puerto_CP;
		$puertoProvinciaNuevo = $this->puerto_Provincia;
		$puertoTelefonoNuevo = $this->puerto_Telefono;
		$puertoFaxNuevo = $this->puerto_Fax;
		$puertoWwwNuevo = $this->puerto_Www;
		$puertoEmailNuevo = $this->puerto_Email;
		$puertoCalado_MinNuevo = $this->puerto_Calado_Min;
		$puertoEslora_MaxNuevo = $this->puerto_Eslora_Max;
		$puertoNamarresNuevo = $this->puerto_Namarres;
		$puertoCanalVhfNuevo = $this->puerto_CanalVhf;
		$puertoServ_AguaNuevo = $this->puerto_Serv_Agua;
		$puertoServ_ElectricidadNuevo = $this->puerto_Serv_Electricidad;
		$puertoServ_GasolinaNuevo = $this->puerto_Serv_Gasolina;
		$puertoServ_GruaNuevo = $this->puerto_Serv_Grua;
		$puertoServ_TravelliftNuevo = $this->puerto_Serv_Travellift;
		$puertoServ_RampaNuevo = $this->puerto_Serv_Rampa;
		$puertoServ_TallerNuevo = $this->puerto_Serv_Taller;
		$puertoServ_MuelleEsperaNuevo = $this->puerto_Serv_MuelleEspera;
		$puertoServ_BarNuevo = $this->puerto_Serv_Bar;
		$puertoServ_RestauranteNuevo = $this->puerto_Serv_Restaurante;
		$puertoServ_SupermercadoNuevo = $this->puerto_Serv_Supermercado;
		$puertoServ_HieloNuevo = $this->puerto_Serv_Hielo;
		$puertoServ_RecogidaAceiteNuevo = $this->puerto_Serv_RecogidaAceite;
		$puertoServ_RecogidaBasuraNuevo = $this->puerto_Serv_RecogidaBasura;
		$puertoServ_AguasNegrasNuevo = $this->puerto_Serv_AguasNegras;
		$puertoServ_FarmaciaNuevo = $this->puerto_Serv_Farmacia;
		$puertoServ_MedicoNuevo = $this->puerto_Serv_Medico;
		$puertoServ_MeteoNuevo = $this->puerto_Serv_Meteo;
		$puertoServ_BancoNuevo = $this->puerto_Serv_Banco;
		$puertoServ_AlquilerCocheNuevo = $this->puerto_Serv_AlquilerCoche;
		$puertoOtrosNuevo = $this->puerto_Otros;
		$puertoDescripcionNuevo = $this->puerto_Descripcion;

		$sql = "UPDATE Puertos SET ";
		$sql.= "nombre='". $puertoNombreNuevo."', latitud='". $puertoLatitudNuevo."', longitud='". $puertoLongitudNuevo."', dir_direccion='". $puertoDireccionNuevo."', dir_poblacion='". $puertoPoblacionNuevo."', dir_codigopostal='". $puertoCPNuevo."', dir_provincia='". $puertoProvinciaNuevo."', telefono='".$puertoTelefonoNuevo."', fax='".$puertoFaxNuevo."', www='".$puertoWwwNuevo."', email='".$puertoEmailNuevo."', calado_min='".$puertoCalado_MinNuevo."', eslora_max='".$puertoEslora_MaxNuevo."', namarres='".$puertoNamarresNuevo."', canalVhf='".$puertoCanalVhfNuevo."', serv_agua='".$puertoServ_AguaNuevo."', serv_electricidad='".$puertoServ_ElectricidadNuevo."', serv_gasolina='".$puertoServ_GasolinaNuevo."', serv_grua='".$puertoServ_GruaNuevo."', serv_Travellift='".$puertoServ_TravelliftNuevo."', serv_rampa='".$puertoServ_RampaNuevo."', serv_taller='".$puertoServ_TallerNuevo."', serv_muelleespera='".$puertoServ_MuelleEsperaNuevo."', serv_bar='".$puertoServ_BarNuevo."', serv_restaurante='".$puertoServ_RestauranteNuevo."', serv_supermercado='".$puertoServ_SupermercadoNuevo."', serv_hielo='".$puertoServ_HieloNuevo."', serv_recogidaaceite='".$puertoServ_RecogidaAceiteNuevo."', serv_recogidabasura='".$puertoServ_RecogidaBasuraNuevo."', serv_aguasnegras='".$puertoServ_AguasNegrasNuevo."', serv_farmacia='".$puertoServ_FarmaciaNuevo."', serv_medico='".$puertoServ_MedicoNuevo."', serv_meteo='".$puertoServ_MeteoNuevo."', serv_banco='".$puertoServ_BancoNuevo."', serv_alquilercoche='".$puertoServ_AlquilerCocheNuevo."', otros='".$puertoOtrosNuevo."', descripcion='".$puertoDescripcionNuevo."' ";
		$sql.= "WHERE id=$pID";

		$rst = $db->ejecutarSQL($sql);

		if ($rst) {
			return 'ok';
		} else {
			return 'ko';
		}
	}
	

	/*
	function eliminarPuerto() {
		$db = new AuxDB();
		$db->conectar();

		$pID = $this->puerto_ID;
		
		$sqlexiste = "SELECT * FROM Puertos WHERE id = '$pID'";
		$pstexiste = $db->ejecutarSQL($sqlexiste);
		$fila = $db->siguienteFila($pstexiste);

		if ($fila) {

			$sql = "DELETE FROM Puertos WHERE id = '$pID'";
			$rst = $db->ejecutarSQL($sql);
			return 'Borrado';
		} else {
			return 'No';
		}
		$db->desconectar();
	}
	*/






}

?>