//
//  InsertarViewController.h
//  PruebaInsertarPunto
//
//  Created by chus on 26/11/12.
//  Copyright (c) 2012 chus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InsertarViewController : UIViewController <UIImagePickerControllerDelegate,       UINavigationControllerDelegate> {
    IBOutlet UISegmentedControl * sc;
    IBOutlet UIImagePickerController *imagenPickerController;
    
}

    @property (weak, nonatomic) IBOutlet UITextField *nombreTextField;
    @property (weak, nonatomic) IBOutlet UITextField *longitudTextField;
    @property (weak, nonatomic) IBOutlet UITextField *latitudTextField;
    @property (weak, nonatomic) IBOutlet UITextView *descripcionTextView;

    @property (weak, nonatomic) UIImage * imagenFoto;
    @property (weak, nonatomic) IBOutlet UIImageView *imagenImagenView;
    @property (strong, nonatomic) IBOutlet UITextField *longitudGradosTextField;
    @property (strong, nonatomic) IBOutlet UITextField *longitudMinutosTextField;
    @property (strong, nonatomic) IBOutlet UITextField *longitudSegundosTextField;
    @property (strong, nonatomic) IBOutlet UITextField *latitudGradosTextField;
    @property (strong, nonatomic) IBOutlet UITextField *latitudMinutosTextField;
    @property (strong, nonatomic) IBOutlet UITextField *latitudSegundosTextField;

    - (IBAction)buscarFoto:(id)sender;
    - (IBAction)insertarButton:(id)sender;
    - (IBAction)subirFoto:(id)sender;
   
    - (IBAction)unidadPushBotom:(id)sender;

    @property (strong, nonatomic) IBOutlet UIButton *unidadDecimalBotom;


@end
