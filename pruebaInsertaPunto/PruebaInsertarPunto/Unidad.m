//
//  Unidad.m
//  PruebaInsertarPunto
//
//  Created by chus on 10/12/12.
//  Copyright (c) 2012 chus. All rights reserved.
//

#import "Unidad.h" 
#import "InsertarViewController.h"

@implementation Unidad

- (NSMutableDictionary *)llenarDatos:datos unidades:(NSInteger)i {
    
    
    if (i==1) { // tenemos que llenar los grados, minutos y segundos
        
        // LONGITUD
        NSArray *a = [[datos objectForKey:@"longitud"] componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@".,"]];
        NSString * parteEntera = [a objectAtIndex:0];
        CGFloat decimal = [[datos objectForKey:@"longitud"] floatValue] - [parteEntera floatValue];
        
        [datos setObject:parteEntera forKey:@"longitudGrados"];
        
        a = [[NSString stringWithFormat:@"%f",decimal * 60] componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@".,"]];
        parteEntera = [a objectAtIndex:0];
        
        [datos setObject:parteEntera forKey:@"longitudMinutos"];
        
        CGFloat decimal2 = (decimal * 60) - [parteEntera floatValue];
        
        a = [[NSString stringWithFormat:@"%f",decimal2 * 60 ] componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@",."]];
        parteEntera = [a objectAtIndex:0];
        
        
        [datos setObject:parteEntera forKey:@"longitudSegundos"];
        
        
      
        // LATITUD
        a = [[datos objectForKey:@"latitud"] componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@".,"]];
        parteEntera = [a objectAtIndex:0];
        decimal = [[datos objectForKey:@"latitud"] floatValue] - [parteEntera floatValue];
        
        [datos setObject:parteEntera forKey:@"latitudGrados"];
        
        a = [[NSString stringWithFormat:@"%f",decimal * 60] componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@".,"]];
        parteEntera = [a objectAtIndex:0];
        
        [datos setObject:parteEntera forKey:@"latitudMinutos"];
        
        decimal2 = (decimal * 60) - [parteEntera floatValue];
        
        a = [[NSString stringWithFormat:@"%f",decimal2 * 60 ] componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@",."]];
        parteEntera = [a objectAtIndex:0];
        
        
        [datos setObject:parteEntera forKey:@"latitudSegundos"];

        
    
    } else { // tenemos que llenar los campos decimales
        
        
        NSString *grLongitud = [datos objectForKey:@"longitudGrados"];
        NSString *miLongitud = [datos objectForKey:@"longitudMinutos"];
        NSString *seLongitud = [datos objectForKey:@"longitudSegundos"];
        
        CGFloat longitudDecimal = [grLongitud floatValue] + ([miLongitud floatValue]*60 + [seLongitud floatValue])/3600;
        
        [datos setObject:[[NSString alloc] initWithFormat:@"%f",longitudDecimal] forKey:@"longitud"];
        
        NSString *grLatitud = [datos objectForKey:@"latitudGrados"];
        NSString *miLatitud = [datos objectForKey:@"latitudMinutos"];
        NSString *seLatitud = [datos objectForKey:@"latitudSegundos"];
        
        CGFloat latitudDecimal = [grLatitud floatValue] + ([miLatitud floatValue]*60 + [seLatitud floatValue])/3600;
        
        [datos setObject:[[NSString alloc] initWithFormat:@"%f",latitudDecimal] forKey:@"latitud"];
        
        
    }
    return datos;
    
}



@end
