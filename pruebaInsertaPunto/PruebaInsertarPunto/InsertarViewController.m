//
//  InsertarViewController.m
//  PruebaInsertarPunto
//
//  Created by chus on 26/11/12.
//  Copyright (c) 2012 chus. All rights reserved.
//

#import "InsertarViewController.h"
#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"
#import "Unidad.h"

@interface InsertarViewController ()
{
    NSString *nombreGenerado;
    NSMutableDictionary *datos;
    NSInteger dec;
    
}

@end

@implementation InsertarViewController

@synthesize imagenFoto;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    nombreGenerado = [[NSString alloc]initWithString:[self generarFoto:10]];
    self.longitudGradosTextField.hidden = YES;
    self.longitudMinutosTextField.hidden = YES;
    self.longitudSegundosTextField.hidden = YES;
    self.latitudMinutosTextField.hidden = YES;
    self.latitudGradosTextField.hidden = YES;
    self.latitudSegundosTextField.hidden = YES;
    
    dec = 1;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buscarFoto:(id)sender {
    imagenPickerController = [[UIImagePickerController alloc]init];
    [imagenPickerController setDelegate:self];
    
    if (sc.selectedSegmentIndex == 0) {
        [imagenPickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    } else {
        [imagenPickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
    }
    [self presentViewController:imagenPickerController animated:YES completion:nil];
    
    
    
}

// Metodo para recuperar la foto
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    imagenFoto = [info objectForKey:UIImagePickerControllerOriginalImage];
    [self.imagenImagenView setImage:imagenFoto];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)insertarButton:(id)sender {
    
    if ([self.nombreTextField.text length] < 3) {
        UIAlertView * alerta = [[UIAlertView alloc]initWithTitle:@"Error Nombre Punto"
                                                         message:@"El nombre del punto tiene que tener más de 3 caracteres"
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alerta show];
        
    } else if ([self.longitudTextField.text floatValue]==0.0 ||
               [self.longitudTextField.text floatValue] >= 90.0 ||
               [self.longitudTextField.text floatValue] <= -90.0) { // valor float no valido
        
        UIAlertView * alerta = [[UIAlertView alloc]initWithTitle:@"Error Longitud"
                                                         message:@"La longitud del punto introducida no es correcta"
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alerta show];
        
    } else if ([self.latitudTextField.text floatValue] <= 0.0 ||
               [self.latitudTextField.text floatValue] >= 180.0) {
        
        UIAlertView * alerta = [[UIAlertView alloc]initWithTitle:@"Error Latitud"
                                                         message:@"La latitud del punto introducida no es correcta"
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alerta show];
    } else if ([self.longitudGradosTextField.text floatValue]==0.0 ||
               [self.longitudGradosTextField.text floatValue]>=90 ||
               [self.longitudGradosTextField.text floatValue] <=-90) {
        
        UIAlertView *alerta = [[UIAlertView alloc]initWithTitle:@"Error Grados de Longitud"
                                                        message:@"Los Grados de longitud no es correcto"
                                                       delegate:nil cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alerta show];
    
    } else if ([self.longitudMinutosTextField.text floatValue]>60) {
        
        UIAlertView *alerta = [[UIAlertView alloc]initWithTitle:@"Error Minutos de Longitud"
                                                        message:@"Los Minutos de longitud no es correcto"
                                                       delegate:nil cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alerta show];
        
    } else if ([self.longitudSegundosTextField.text floatValue]>60) {
        
        UIAlertView *alerta = [[UIAlertView alloc]initWithTitle:@"Error Segundos de Longitud"
                                                        message:@"Los Segundos de longitud no es correcto"
                                                       delegate:nil cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alerta show];
        
    } else if ([self.latitudGradosTextField.text floatValue]==0.0 ||
               [self.latitudGradosTextField.text floatValue]>=90 ||
               [self.latitudGradosTextField.text floatValue] <=-90) {
        
        UIAlertView *alerta = [[UIAlertView alloc]initWithTitle:@"Error Grados de latitud"
                                                        message:@"Los Grados de latitud no es correcto"
                                                       delegate:nil cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alerta show];
        
    } else if ([self.latitudMinutosTextField.text floatValue]>60) {
        
        UIAlertView *alerta = [[UIAlertView alloc]initWithTitle:@"Error Minutos de latitud"
                                                        message:@"Los Minutos de latitud no es correcto"
                                                       delegate:nil cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alerta show];
        
    } else if ([self.latitudSegundosTextField.text floatValue]>60) {
        
        UIAlertView *alerta = [[UIAlertView alloc]initWithTitle:@"Error Segundos de latitud"
                                                        message:@"Los Segundos de latitud no es correcto"
                                                       delegate:nil cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alerta show];
        
    } else {
        
    
        [self subirFoto:self];
        
        
        Unidad * unidad = [[Unidad alloc]init];  
    
        datos  = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                  self.nombreTextField.text,           @"nombre",
                  self.longitudTextField.text,         @"longitud",
                  self.latitudTextField.text,          @"latitud",
                  self.longitudGradosTextField.text,   @"longitudGrados",
                  self.longitudMinutosTextField.text,  @"longitudMinutos",
                  self.longitudSegundosTextField.text, @"longitudSegundos",
                  self.latitudGradosTextField.text,    @"latitudGrados",
                  self.latitudMinutosTextField.text,   @"latitudMinutos",
                  self.latitudSegundosTextField.text,  @"latitudSegundos",
                  self.descripcionTextView.text,       @"descripcion",nil];
          
        datos = [unidad llenarDatos:datos unidades:dec];
    
    
        NSURL * url = [NSURL URLWithString:@"http://www.marinaferry.es/sandbox/iBarco/JSON/"];
        AFHTTPClient * httpCliente = [[AFHTTPClient alloc]initWithBaseURL:url];
        NSString * phpArchivo = [[NSString alloc]initWithFormat:@"insertarPunto.php?nombreFoto=%@.jpg", nombreGenerado];

        NSURLRequest * request = [httpCliente requestWithMethod:@"POST" path:phpArchivo parameters:datos];
    
        // si no pongo esta linea me da un error en la llamada editarOperacion????
        [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    
    
        // ESTO HACE LA OPERACION:
        // EN JSON ESTA LA SALIDA DE LA LLAMADA AL PHP EN JSON.
        // PUEDO PROCESAR ESTA SALIDA PARA DAR INFORMACION DE LA INSERCION
        AFJSONRequestOperation * editarOperacion = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest * request, NSHTTPURLResponse * response, id JSON) {

            // Procesamiento de JSON en caso correcto
            NSData *dat = [NSJSONSerialization dataWithJSONObject:JSON options:0 error:nil];
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:dat options:0 error:nil];
            for (NSDictionary *valor in dic) {
                NSLog(@"Resultado: %@", [valor objectForKey:@"salida"]);
            }

        
        } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error, id JSON) {
            NSLog(@"error:::: %@", [error debugDescription]);
            NSData *dat = [NSJSONSerialization dataWithJSONObject:JSON options:0 error:nil];
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:dat options:0 error:nil];
            for (NSDictionary *valor in dic) {
                NSLog(@"Resultado: %@", [valor objectForKey:@"salida"]);
            }
        }];
    
        [editarOperacion start];
        
    
    }
    
 
}


// Genera un nombre para el fichero de la foto, que guarda en el servidor
- (NSString *)generarFoto: (int) len {
    
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
            [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
    }
        
        return randomString;
}


- (IBAction)subirFoto:(id)sender {
    // ponemos la imagen dentro de NSData y  ponemos la calidad a 90% y probando
    NSData *imagenData = UIImageJPEGRepresentation(imagenFoto, 90);
    // URL para el POST
    NSString * urlString =@"http://www.marinaferry.es/sandbox/iBarco/upload.php";
    // Aqui ponemos la llamada al request
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc]init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    // ponemos un poco de informacion de la cabecera
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    // ahora creamos el body para el post
    NSMutableData * body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary]dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString * nf = [[NSString alloc]initWithFormat:@"Content-Disposition: form-data; name=\'userfile\'; filename=%@.jpg  \r\n", nombreGenerado];
    
    
    // Aqui va el nombre del fichero. Tenemos que pensar que nombre le ponemos para que sea facil recuperar
    [body appendData:[nf dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:[@"Content-Type; application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imagenData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // ponemos el body del post en el request
    [request setHTTPBody:body];
    
    // ahora hacemos la conexion con la web
    NSData * returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error: nil];
    NSString *returnString = [[NSString alloc]initWithData:returnData encoding:NSUTF8StringEncoding];
    NSLog(@"Retorno : %@",returnString);
    
    if ([returnString isEqualToString:@"\nOk"]) {
        UIAlertView * correcto = [[UIAlertView alloc]initWithTitle:@"Fichero subido"
                                                           message:@"El fichero ha sido subido correctamente"
                                                          delegate:nil
                                                 cancelButtonTitle:@"Ok"
                                                 otherButtonTitles:nil];
        [correcto show];
    } else {
        UIAlertView * incorrecto = [[UIAlertView alloc]initWithTitle:@"Fichero NO subido"
                                                           message:@"El fichero no ha sido subido correctamente"
                                                          delegate:nil
                                                 cancelButtonTitle:@"Ok"
                                                 otherButtonTitles:nil];
        [incorrecto show];
    }
    
}


- (IBAction)unidadPushBotom:(id)sender {
    
    Unidad * unidad = [[Unidad alloc]init];
    
    datos  = [NSMutableDictionary dictionaryWithObjectsAndKeys:
              self.nombreTextField.text,           @"nombre",
              self.longitudTextField.text,         @"longitud",
              self.latitudTextField.text,          @"latitud",
              self.descripcionTextView.text,       @"descripcion",
              self.longitudGradosTextField.text,   @"longitudGrados",
              self.longitudMinutosTextField.text,  @"longitudMinutos",
              self.longitudSegundosTextField.text, @"longitudSegundos",
              self.latitudGradosTextField.text,    @"latitudGrados",
              self.latitudMinutosTextField.text,   @"latitudMinutos",
              self.latitudSegundosTextField.text,  @"latitudSegundos", nil];
    
    datos = [unidad llenarDatos:datos unidades:dec];
    
    self.longitudTextField.text = [datos objectForKey:@"longitud"];
    self.latitudTextField.text =  [datos objectForKey:@"latitud"];
    
    self.longitudGradosTextField.text =     [datos objectForKey:@"longitudGrados"];
    self.longitudMinutosTextField.text =    [datos objectForKey:@"longitudMinutos"];
    self.longitudSegundosTextField.text =   [datos objectForKey:@"longitudSegundos"];
    
    self.latitudGradosTextField.text =    [datos objectForKey:@"latitudGrados"];
    self.latitudMinutosTextField.text =   [datos objectForKey:@"latitudMinutos"];
    self.latitudSegundosTextField.text =  [datos objectForKey:@"latitudSegundos"];
    
    if (dec == 1) {
        [self.unidadDecimalBotom setTitle:@"Grados/minutos/segundos" forState: 0];
        self.longitudGradosTextField.hidden = NO;
        self.longitudMinutosTextField.hidden = NO;
        self.longitudSegundosTextField.hidden = NO;
        self.latitudGradosTextField.hidden = NO;
        self.latitudMinutosTextField.hidden = NO;
        self.latitudSegundosTextField.hidden = NO;
        
        self.longitudTextField.hidden = YES;
        self.latitudTextField.hidden = YES;
        
        dec = 2;
    } else {
        
        [self.unidadDecimalBotom setTitle:@"Decimal" forState: 0];
        self.longitudGradosTextField.hidden = YES;
        self.longitudMinutosTextField.hidden = YES;
        self.longitudSegundosTextField.hidden = YES;
        self.latitudGradosTextField.hidden = YES;
        self.latitudMinutosTextField.hidden = YES;
        self.latitudSegundosTextField.hidden = YES;
        
        self.longitudTextField.hidden = NO;
        self.latitudTextField.hidden = NO;
        
        dec = 1;
    }

    
}
@end
