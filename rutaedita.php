<?php
include "clRuta.php";
//Ejecutara la accion de modificar

if (isset($_POST['enviar'])) {
	// creamos una nueva ruta
	$oRuta = new Ruta($_POST["id"],$_POST["nombre"],$_POST["descripcion"],$_POST["distancia"]);
}

?>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>iBarco</title>
	<link href="css/styles.css" rel="stylesheet" type="text/css" />
	<script src="js/js.js"></script>
</head>
<body>
	<div class="titulo"><a class="titulo" href="index.html">iBarco</a></div>
	<br><br>
	<form method="post" id="formRuta" action="rutamodificar.php">
		<fieldset>
			<legend class="titulo">Editando datos de Ruta</legend>   
			<div>
				<label for="nombre">Nombre</label>
				<input type="text" class="grande" id="nombre" name="nombre" value="<?php echo $oRuta->getRutaNombre(); ?>"/>
			</div>
			<br />
			<div>
				<label for="latitud">Descripcion</label>
				<input type="text" class="grande" id="descripcion" name="descripcion" value="<?php echo $oRuta->getRutaDescripcion(); ?>"/>
			</div>
			<br />
			<div>
				<label for="longitud">Distancia</label>
				<input type="text" class="corto" id="distancia" name="distancia" value="<?php echo $oRuta->getRutaDistancia(); ?>" />
			</div>
			<br />
			<div>

				<input type="hidden" id="id" name="id" value="<?php echo $oRuta->getRutaID(); ?>" />
				<input type="submit" class="btn" value="Guardar" name="enviar"/>
				<input type="button" class="btn" value="Limpiar" name="limpiar"/>
				<input type="submit" class="btn" value="Eliminar" name="eliminar"/>
				<!--<input type="button" onClick="javascript:history.back();" value="Cancelar" name="Cancelar"/>-->
				<!--<input type="button" class="btn" value="Eliminar" value="Eliminar" name="Eliminar" onClick="javascript:editar('rutaeliminar.php?id=<?php echo $oRuta->getRutaID();?>');">-->
				
			</div>
		</fieldset>
	</form>
</body>
</html>