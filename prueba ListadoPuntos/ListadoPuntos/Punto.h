//
//  Punto.h
//  ListadoPuntos
//
//  Created by chus on 22/11/12.
//  Copyright (c) 2012 chus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Punto : NSObject

@property (nonatomic, strong) NSString *nombre;
@property (nonatomic, strong) NSString *longitud;
@property (nonatomic, strong) NSString *latitud;
@property (nonatomic, strong) NSString *descripcion;
@property (nonatomic, strong) NSString *foto;


@end
