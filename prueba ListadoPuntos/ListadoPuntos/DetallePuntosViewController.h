//
//  DetallePuntosViewController.h
//  ListadoPuntos
//
//  Created by chus on 22/11/12.
//  Copyright (c) 2012 chus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Punto.h"

@interface DetallePuntosViewController : UIViewController

@property (nonatomic,strong) Punto *punto;

@property (weak, nonatomic) IBOutlet UILabel *nombreLabel;
@property (weak, nonatomic) IBOutlet UILabel *longitudLabel;
@property (weak, nonatomic) IBOutlet UILabel *latitudLabel;
@property (weak, nonatomic) IBOutlet UILabel *descripcionLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imagenImageView;

@end
