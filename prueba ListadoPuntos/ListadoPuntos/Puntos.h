//
//  Puntos.h
//  ListadoPuntos
//
//  Created by chus on 24/11/12.
//  Copyright (c) 2012 chus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Puntos : NSObject

@property (nonatomic, strong) NSMutableArray *nombre;
@property (nonatomic, strong) NSMutableArray *longitud;
@property (nonatomic, strong) NSMutableArray *latitud;
@property (nonatomic, strong) NSMutableArray *descripcion;
@property (nonatomic, strong) NSMutableArray *foto;

@end
