//
//  AppDelegate.h
//  ListadoPuntos
//
//  Created by chus on 22/11/12.
//  Copyright (c) 2012 chus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
