//
//  Conexion.h
//  ListadoPuntos
//
//  Created by chus on 23/11/12.
//  Copyright (c) 2012 chus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Conexion : NSObject {
    NSURLConnection *conexion;
}

-(NSDictionary *) datos;

@end
