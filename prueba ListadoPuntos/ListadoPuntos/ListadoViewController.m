//
//  ListadoViewController.m
//  ListadoPuntos
//
//  Created by chus on 22/11/12.
//  Copyright (c) 2012 chus. All rights reserved.
//

#import "ListadoViewController.h"
#import "DetallePuntosViewController.h"
#import "Punto.h"
#import "Conexion.h"

@interface ListadoViewController ()

@end

@implementation ListadoViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSError *error = nil;

    //---------------------------------------------------------


    
    // Hago la conexion con el servidor y realizo la llamada al JSON
    // En dicc tengo todos los datos en JSON de la llamada al servidor
    
    Conexion * con = [[Conexion alloc]init];
    NSDictionary * dicc = [[NSDictionary alloc]init];
    dicc = [con datos];
    
    // puntosArray es el array de puntos que se envia al textView
        puntosArray = [[NSMutableArray alloc]init];

    if (error != nil) {
        NSLog(@"Error : %@", [error localizedDescription]);
    } else {       
       // Recorro el dicc y voy poniendo los puntos en puntosArray
            for (NSDictionary *condicion in dicc) {
                Punto *punto = [[Punto alloc]init];

            // Aqui es donde falla:
            //NSLog(@"Nombre: %@",[condicion objectForKey:@"nombre"] );
            
                punto.nombre =  [condicion objectForKey:@"nombre"];
                punto.longitud = [condicion objectForKey:@"longitud"];
                punto.latitud =  [condicion objectForKey:@"latitud"];
                punto.descripcion =  [condicion objectForKey:@"descripcion"];
                punto.foto =  [condicion objectForKey:@"fotos"];
            
            // meto los puntos en puntoArray
                [puntosArray addObject:punto];
            }
    }  

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return [puntosArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    Punto *puntoAuxiliar = [puntosArray objectAtIndex:indexPath.row];
    cell.textLabel.text = puntoAuxiliar.nombre;
    cell.detailTextLabel.text = @"Aquí se puede poner un detalle del punto";
    cell.tag = indexPath.row;
    
    return cell;
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"goToDetallePunto"]) {
        UITableViewCell *cell = (UITableViewCell *)sender;
        DetallePuntosViewController *detallePuntosViewController = (DetallePuntosViewController *)segue.destinationViewController;
        detallePuntosViewController.punto = [puntosArray objectAtIndex:cell.tag];
        
    }
}

@end
