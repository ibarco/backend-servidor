//
//  main.m
//  ListadoPuntos
//
//  Created by chus on 22/11/12.
//  Copyright (c) 2012 chus. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
