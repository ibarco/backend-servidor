//
//  DetallePuntosViewController.m
//  ListadoPuntos
//
//  Created by chus on 22/11/12.
//  Copyright (c) 2012 chus. All rights reserved.
//

#import "DetallePuntosViewController.h"

@interface DetallePuntosViewController ()

@end

@implementation DetallePuntosViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }

    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.nombreLabel.text = self.punto.nombre;
    self.longitudLabel.text = self.punto.longitud;
    self.latitudLabel.text = self.punto.latitud;
    self.descripcionLabel.text = self.punto.descripcion;
  //  UIImage *im = [[UIImage alloc]initWithContentsOfFile:self.punto.foto];
    NSString *stringURL = [[NSString alloc]initWithFormat:@"http://www.marinaferry.es/sandbox/iBarco/%@",self.punto.foto ];
    NSURL *url = [[NSURL alloc]initWithString:stringURL];
    NSData *imagen = [[NSData alloc]initWithContentsOfURL:url];
    UIImage *im = [[UIImage alloc]initWithData:imagen];
    [self.imagenImageView setImage:im];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
