<html>
<head>
	<meta charset="UTF-8">
	<title>iBarco - Puntos de Interés</title>
	<link href="css/styles.css" rel="stylesheet" type="text/css" />
	
</head>

<body>

	<div class="titulo">iBarco - Puntos de Interés</div>
	<br><br>
	<fieldset>
		<legend class="titulo">Apartados</legend> 
		<div>
			<a class="menu" href="formularioPuntos.php">Alta Punto de Interés</a>
		</div>
		<br>
		<div>
			<a class="menu" href="formularioBajaPuntos.php">Baja Punto de Interés</a>
		</div>
		<br>
		<div>
			<a class="menu" href="tablaPuntosInteres.php">Tabla Punto de Interés</a>
			<!--<a class='menu' href='JSON/puntos.php'>Puntos de Interés</a>-->
		</div>
	</fieldset>
	<a class='menu' href='index.html'>Volver a índice general</a>
</body>
</html>
