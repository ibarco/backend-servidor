<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html"; charset="utf-8" />
    <title>Formulario para valoración de una ruta</title>
</head>
<body>
<?php

session_start();
$ruta = $_POST['id'];
// Aun no hemos rellenado el formulario
if (!isset($_POST['enviar'])) {?>

    <div>
        <form method="POST" action="JSON/valorarRutas.php">
            <fieldset>
                <legend>Valorar <?php printf($ruta);?>:</legend>   
            	   <div>
                		<label for="valorarion">Valoración:(1-5)</label>
                		<input type='text' name='valoracion'/>
            	   </div>
                </br>
            	   <div>
            		  <label for="comentario">Comentario:</label>
            		  <input type='text' name='comentario'/>
            	   </div>
                </br>
                      <input type='hidden' name='id' value='<?php printf($ruta); ?>' />
            	      <input type="submit" class="btn" value="Aceptar" name="enviar"/>
            </fieldset>
        </form>
    </div>
<?php   
} 