<?php

include "clPuntos.php";
// si recibimos parámetros desde post
if (isset($_POST['enviar'])) {

	// creamos un nuevo Punto 
	$oPunto = new Punto('',$_POST['nombre'],
						$_POST['longitud'],
						$_POST['latitud'],
						$_POST['descripcion'],
						$_POST['fotos']);


	// validamos el campo Nombre para que no esté repetido
	$error = $oPunto->validarPunto();
	if ($error != $oPunto->getID()) { //Punto ya existente
		printf($error);
	} else {
		printf($oPunto->getId);
		$oPunto->insertarPunto();
	}

}?>
<html>
<head>
	<title></title>
</head>
<body>
	<a href='indexPuntos.php'>Volver</a>
</body>
</html>



