<?php

require_once "AuxDB.php";
$url = '';
$clave = '';
// clase Puntos de Interés

class Punto {
	
	private $id;
	private $nombre;
	private $longitud;
	private $latitud;
	private $longitudGrados;
	private $longitudMinutos;
	private $longitudSegundos;
	private $latitudGrados;
	private $latitudMinutos;
	private $latitudSegundos;
	private $descripcion;
	private $fotos;
	
	
	// Funciones de la clase Usuario
	function getId() {
		return $this->id;
	}
	function getNombre(){
		return $this->nombre;
	}
	function getLongitud() {
		return $this->longitud;
	}
	function getLatitud() {
		return $this->latitud;
	}
	function getLongitudGrados() {
		return $this->longitudGrados;
	}
	function getLongitudMinutos() {
		return $this->longitudMinutos;
	}
	function getLongitudSegundos() {
		return $this->longitudSegundos;
	}
	function getLatitudGrados() {
		return $this->latitudGrados;
	}
	function getLatitudMinutos() {
		return $this->latitudMinutos;
	}
	function getLatitudSegundos() {
		return $this->latitudSegundos;
	}
	function getDescripcion() {
		return $this->descripcion;
	}
	function getFotos() {
		return $this->fotos;
	}
	
	function __Construct($id, $nom, $lon, $lat, $lonG, $lonM, $lonS, $latG, $latM, $latS, $des, $fot) {
		
	 // Es el contructor del objeto Punto
		$this->id = $id;
		$this->nombre = $nom;
		$this->longitud = $lon;		
		$this->latitud = $lat;
		$this->longitudGrados = $lonG;
		$this->longitudMinutos = $lonM;
		$this->longitudSegundos = $lonS;
		$this->latitudGrados = $latG;
		$this->latitudMinutos = $latM;
		$this->latitudSegundos = $latS;
		$this->descripcion = $des;
		$this->fotos = $fot;
	}

	// Lee un punto a partir de su ID
	function leerPunto() {

		$db = new AuxDB();
		$db->conectar();

		$sql = "SELECT * FROM Puntos WHERE id = '" . $this->id . "'";
		
		$rst = $db->ejecutarSQL($sql);
		
		$fila = $db->siguienteFila($rst);
		
		$this->nombre = $fila['Nombre'];
		$this->longitud = $fila['Longitud'];
		$this->latitud = $fila['Latitud'];
		$this->longitudGrados = $fila['longitudGrados'];
		$this->longitudMinutos = $fila['longitudMinutos'];
		$this->longitudSegundos = $fila['longitudSegundos'];
		$this->latitudGrados = $fila['latitudGrados'];
		$this->latitudMinutos = $fila['latitudMinutos'];
		$this->latitudSegundos = $fila['latitudSegundos'];
		$this->descripcion = $fila['Descripcion'];
		$this->fotos = $fila['Fotos'];

		$db->desconectar();
		
	}

	// Lee un Punto a partir de su nombre
	function leerPuntoNombre($nom) {

		$db = new AuxDB();
		$db->conectar();
		$sql = "SELECT * FROM Puntos WHERE Nombre = '" . $nom . "'";
		$rst = $db->ejecutarSQL($sql);
		
		$fila = $db->siguienteFila($rst);
		
		$this->id = $fila['id'];
		$this->nombre = $fila['Nombre'];
		$this->longitud = $fila['Longitud'];
		$this->latitud = $fila['Latitud'];
		$this->longitudGrados = $fila['longitudGrados'];
		$this->longitudMinutos = $fila['longitudMinutos'];
		$this->longitudSegundos = $fila['longitudSegundos'];
		$this->latitudGrados = $fila['latitudGrados'];
		$this->latitudMinutos = $fila['latitudMinutos'];
		$this->latitudSegundos = $fila['latitudSegundos'];
		$this->descripcion = $fila['Descripcion'];
		$this->fotos = $fila['Fotos'];

		$db->desconectar();
	}


	// Inserta un Punto de interes en la tabla Puntos
	// 1->insercion correcta
	// 0->error en insercion
	function insertarPunto() {

		$db=new AuxDB();
		$db->conectar();
		
		$sql = "INSERT INTO Puntos (Nombre, 
									Longitud, 
									Latitud, 
									longitudGrados, 
									longitudMinutos, 
									longitudSegundos, 
									latitudGrados,
									latitudMinutos,
									latitudSegundos,
									Descripcion, 
									Fotos) ";

		$sql.= "VALUES ('".mysql_escape_string($this->nombre)."', '". 
											   $this->longitud."', '". 
											   $this->latitud ."', '".
											   $this->longitudGrados ."', '".
											   $this->longitudMinutos ."', '".
											   $this->longitudSegundos ."', '".
											   $this->latitudGrados ."', '".
											   $this->latitudMinutos ."', '".
											   $this->latitudSegundos."', '";

		$sql.= mysql_escape_string($this->descripcion)."','". 
			   mysql_escape_string($this->fotos)."')";

		$db->ejecutarSQL($sql);
		$ret = 0;

		if ($db) { //insercion correcta
			$ret = 1;
		}

		$db->desconectar();
		return $ret;
	} 

	// No se si este método será necesario
	function bajaPunto() {

		$db = new AuxDB();
		$db->conectar();

		$id = $this->id;

		$sqlexiste = "SELECT * FROM Puntos WHERE id = '$id'";
		$rstexiste = $db->ejecutarSQL($sqlexiste);
		$fila = $db->siguienteFila($rstexiste);

		if ($fila) {
			$sql = "DELETE FROM Puntos WHERE id = '$id'";
			$rst = $db->ejecutarSQL($sql);
			return 'Borrado';
		} else {
			return 'No';
		}

		$db->desconectar();
	}

	//retorna :'existe' si el punto ya esta introducido
	//		  : id si no existe
	//'ErrorX': en caso de error en la introduccion de datos
	function validarPunto() {

		$permitidosNombre = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_ ";
		$error = '';
		// miramos si existe en la tabla Puntos
		$db = new AuxDB();
		$db->conectar();

		$sql = "SELECT Nombre  from Puntos where Nombre = '".$this->nombre."'";
		
		$res = $db->ejecutarSQL($sql);
		
		$fila = $db->siguienteFila($res);

		$db->desconectar();

		if ($fila) {
			$error.= 'existe';
		}
		// vemos si los caracteres del nombre estan permitidos
		for ($i=0; $i<strlen($this->nombre); $i++){
			    if (strpos($permitidosNombre, substr($this->nombre,$i,1))===false) {
	 		   	  $error.='errorC';
	 		   	}
		}

		// validamos los campos longitud y latitud para que sean float
		if (!is_numeric($this->longitud)) {
			$error.='long';
		}

		if (!is_numeric($this->latitud)) {
			$error.='lat';
		}

		// validamos el campo descripcion
		for ($i=0; $i<strlen($this->descripcin); $i++){
			    if (strpos($permitidos, substr($this->descripcion,$i,1))===false) {
	 		   	  $error.='errorD';
	 		   	}
		}
		// validamos el campo foto
		for ($i=0; $i<strlen($this->foto); $i++){
			    if (strpos($permitidos, substr($this->foto,$i,1))===false) {
	 		   	  $error.='errorF';
	 		   	}
		}

		// retorna el dato de la funcion
		if ($error != '') {
			return $error;
		} else {
			return ($this->id);
		}

	}


}
?>