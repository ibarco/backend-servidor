<?php
include "AuxDB.php";
require("funciones.php");

$idpuerto= getParam($_GET["id"], "-1");

$sql = "SELECT * FROM Puertos WHERE id = ".sqlValue($idpuerto, "int");
//Establecemos conexión con la BD
$db = new AuxDB();
$db->conectar();
//Ejecutamos la consulta SQL
$result = $db->ejecutarSQL($sql);
$row = $db->siguienteFila($result);

$total = $db->cantidadFilas($result);
if ($total == 0) {
	header("location: puertos.php");
	exit;
}
?>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Puerto</title>
	<link href="css/styles.css" rel="stylesheet" type="text/css" />
	<script src="js/js.js"></script>
</head>
<body>
	<div class="titulo"><a class="titulo" href="index.html">iBarco</a></div>
	<br><br>
	<form method="post" id="formPuerto" action="puertoedita.php">
		<fieldset>
			<legend class="titulo">Visualizando datos de Puerto</legend>   
			<div>
				<label for="nombre">Nombre</label>
				<input type="text" class="grande" id="nombre" name="nombre" value="<?php echo $row["nombre"]; ?>" readonly/>
			</div>
			<br />
			<div class="camposjuntos">
				<label for="latitud">Latitud</label>
				<input type="text" class="corto" id="latitud" name="latitud" value="<?php echo $row["latitud"]; ?>"  readonly/>
			</div>
			<div class="camposjuntos">
				<label for="longitud">Longitud</label>
				<input type="text" class="corto" id="longitud" name="longitud" value="<?php echo $row["longitud"]; ?>"  readonly/>
			</div>

			<br />
			<div class="contenedor">
				<br />
			</div>	
			<div>
				<label for="direccion">Direccion</label>
				<input type="text" class="corto" id="direccion" name="direccion" value="<?php echo $row["dir_direccion"]; ?>"  readonly/>
			</div>
			<br />
			<div>
				<label for="poblacion">Poblacion</label>
				<input type="text" class="corto" id="poblacion" name="poblacion" value="<?php echo $row["dir_poblacion"]; ?>"  readonly/>
			</div>
			<br />
			<div>
				<label for="cp">CP</label>
				<input type="text" class="corto" id="cp" name="cp" value="<?php echo $row["dir_codigopostal"]; ?>"  readonly/>
			</div>
			<br />
			<div>
				<label for="provincia">Provincia</label>
				<input type="text" class="corto" id="provincia" name="provincia" value="<?php echo $row["dir_provincia"]; ?>"  readonly/>
			</div>
			<br />
			<div>
				<label for="telefono">Telefono</label>
				<input type="text" class="corto" id="telefono" name="telefono" value="<?php echo $row["telefono"]; ?>"  readonly/>
			</div>
			<br />
			<div>
				<label for="fax">Fax</label>
				<input type="text" class="corto" id="fax" name="fax" value="<?php echo $row["fax"]; ?>"  readonly/>
			</div>
			<br />
			<div>
				<label for="www">Www</label>
				<input type="text" class="corto" id="www" name="www" value="<?php echo $row["www"]; ?>"  readonly/>
			</div>
			<br />
			<div>
				<label for="email">Email</label>
				<input type="text" class="corto" id="email" name="email" value="<?php echo $row["email"]; ?>"  readonly/>
			</div>
			<br />
			<div>
				<label for="caladomin">Calado minimo</label>
				<input type="text" class="corto" id="caladomin" name="caladomin" value="<?php echo $row["calado_min"]; ?>"  readonly/>
			</div>
			<br />
			<div>
				<label for="esloramax">Eslora maxima</label>
				<input type="text" class="corto" id="esloramax" name="esloramax" value="<?php echo $row["eslora_max"]; ?>"  readonly/>
			</div>
			<br />
			<div>
				<label for="namarres">N de amarres</label>
				<input type="text" class="corto" id="namarres" name="namarres" value="<?php echo $row["namarres"]; ?>"  readonly/>
			</div>
			<br />
			<div>
				<label for="canalvhf">Canal vhf</label>
				<input type="text" class="corto" id="canalvhf" name="canalvhf" value="<?php echo $row["canalvhf"]; ?>"  readonly/>
			</div>
			<br />
			<div class="camposjuntos">
				<label for="servagua2">Serv. Agua</label>
				<input type="checkbox" name="servagua2" value="1"
				<?php if($row["serv_agua"] == "1"){
					echo("CHECKED");
				}?>
				DISABLED/>
				<input type="hidden" id="servagua" name="servagua" value="<?php echo $row["serv_agua"]; ?>"  />
			</div>

			<div class="camposjuntos">
				<label for="servelectricidad2">Serv. Electricidad</label>
				<input type="checkbox" name="servelectricidad2" value="1"
				<?php if($row["serv_electricidad"] == "1"){
					echo("CHECKED");
				}?>
				 DISABLED/>
				 <input type="hidden" id="servelectricidad" name="servelectricidad" value="<?php echo $row["serv_electricidad"]; ?>" />
			</div>

			<div  class="camposjuntos">
				<label for="servgasolina2">Serv. Gasolina</label>
				<input type="checkbox" name="servgasolina2" value="1"
				<?php if($row["serv_gasolina"] == "1"){
					echo("CHECKED");
				}?>
				DISABLED/>
				<input type="hidden" id="servgasolina" name="servgasolina" value="<?php echo $row["serv_gasolina"]; ?>" />
			</div>
			
			<div class="camposjuntos">
				<label for="servgrua2">Serv. Grua</label>
				<input type="checkbox" name="servgrua2" value="1"
				<?php if($row["serv_grua"] == "1"){
					echo("CHECKED");
				}?>
				DISABLED/>
				<input type="hidden" id="servgrua" name="servgrua" value="<?php echo $row["serv_grua"]; ?>" />
			</div>
			
			<div class="camposjuntos">
				<label for="servtravellift2">Serv. travellift</label>
				<input type="checkbox" name="servtravellift2" value="1"
				<?php if($row["serv_travellift"] == "1"){
					echo("CHECKED");
				}?>
				DISABLED/>
				<input type="hidden" id="servtravellift" name="servtravellift" value="<?php echo $row["serv_travellift"]; ?>" />
			</div>
			
			<div class="camposjuntos">
				<label for="servrampa2">Serv. rampa</label>
				<input type="checkbox" name="servrampa2" value="1"
				<?php if($row["serv_rampa"] == "1"){
					echo("CHECKED");
				}?>
				DISABLED/>
				<input type="hidden" id="servrampa" name="servrampa" value="<?php echo $row["serv_rampa"]; ?>" />
			</div>
			
			<div class="camposjuntos">
				<label for="servtaller2">Serv. taller</label>
				<input type="checkbox" name="servtaller2" value="1"
				<?php if($row["serv_taller"] == "1"){
					echo("CHECKED");
				}?>
				DISABLED/>
				<input type="hidden" id="servtaller" name="servtaller" value="<?php echo $row["serv_taller"]; ?>" />
			</div>
			
			<div class="camposjuntos">
				<label for="servmuelleespera2">Serv. muelle de espera</label>
				<input type="checkbox" name="servmuelleespera2" value="1"
				<?php if($row["serv_muelleespera"] == "1"){
					echo("CHECKED");
				}?>
				DISABLED/>
				<input type="hidden" id="servmuelleespera" name="servmuelleespera" value="<?php echo $row["serv_muelleespera"]; ?>" />
			</div>
			
			<div class="camposjuntos">
				<label for="servbar2">Serv. Bar</label>
				<input type="checkbox" name="servbar2" value="1"
				<?php if($row["serv_bar"] == "1"){
					echo("CHECKED");
				}?>
				DISABLED/>
				<input type="hidden" id="servbar" name="servbar" value="<?php echo $row["serv_bar"]; ?>" />
			</div>
			
			<div class="camposjuntos">
				<label for="servrestaurante2">Serv. restaurante</label>
				<input type="checkbox" name="servrestaurante2" value="1"
				<?php if($row["serv_restaurante"] == "1"){
					echo("CHECKED");
				}?>
				DISABLED/>
				<input type="hidden" id="servrestaurante" name="servrestaurante" value="<?php echo $row["serv_restaurante"]; ?>" />
			</div>
			
			<div class="camposjuntos">
				<label for="servsupermercado2">Serv. supermercado</label>
				<input type="checkbox" name="servsupermercado2" value="1"
				<?php if($row["serv_supermercado"] == "1"){
					echo("CHECKED");
				}?>
				DISABLED/>
				<input type="hidden" id="servsupermercado" name="servsupermercado" value="<?php echo $row["serv_supermercado"]; ?>" />
			</div>
			
			<div class="camposjuntos">
				<label for="servhielo2">Serv. hielo</label>
				<input type="checkbox" name="servhielo2" value="1"
				<?php if($row["serv_hielo"] == "1"){
					echo("CHECKED");
				}?>
				DISABLED/>
				<input type="hidden" id="servhielo" name="servhielo" value="<?php echo $row["serv_hielo"]; ?>" />
			</div>
			
			<div class="camposjuntos">
				<label for="servrecogidaaceite2">Serv. recogida aceite</label>
				<input type="checkbox" name="servrecogidaaceite2" value="1"
				<?php if($row["serv_recogidaaceite"] == "1"){
					echo("CHECKED");
				}?>
				DISABLED/>
				<input type="hidden" id="servrecogidaaceite" name="servrecogidaaceite" value="<?php echo $row["serv_recogidaaceite"]; ?>" />
			</div>
			
			<div class="camposjuntos">
				<label for="servrecogidabasura2">Serv. recogida basura</label>
				<input type="checkbox" name="servrecogidabasura2" value="1"
				<?php if($row["serv_recogidabasura"] == "1"){
					echo("CHECKED");
				}?>
				DISABLED/>
				<input type="hidden" id="servrecogidabasura" name="servrecogidabasura" value="<?php echo $row["serv_recogidabasura"]; ?>" />
			</div>
			
			<div class="camposjuntos">
				<label for="servaguasnegras2">Serv. aguas negras</label>
				<input type="checkbox" name="servaguasnegras2" value="1"
				<?php if($row["serv_aguasnegras"] == "1"){
					echo("CHECKED");
				}?>
				DISABLED/>
				<input type="hidden" id="servaguasnegras" name="servaguasnegras" value="<?php echo $row["serv_aguasnegras"]; ?>" />
			</div>
			
			<div class="camposjuntos">
				<label for="servfarmacia2">Serv. farmacia</label>
				<input type="checkbox" name="servfarmacia2" value="1"
				<?php if($row["serv_farmacia"] == "1"){
					echo("CHECKED");
				}?>
				DISABLED/>
				<input type="hidden" id="servfarmacia" name="servfarmacia" value="<?php echo $row["serv_farmacia"]; ?>" />
			</div>
			
			<div class="camposjuntos">
				<label for="servmedico2">Serv. medico</label>
				<input type="checkbox" name="servmedico2" value="1"
				<?php if($row["serv_medico"] == "1"){
					echo("CHECKED");
				}?>
				DISABLED/>
				<input type="hidden" id="servmedico" name="servmedico" value="<?php echo $row["serv_medico"]; ?>" />
			</div>
			
			<div class="camposjuntos">
				<label for="servmeteo2">Serv. meteo</label>
				<input type="checkbox" name="servmeteo2" value="1"
				<?php if($row["serv_meteo"] == "1"){
					echo("CHECKED");
				}?>
				DISABLED/>
				<input type="hidden" id="servmeteo" name="servmeteo" value="<?php echo $row["serv_meteo"]; ?>" />
			</div>
			
			<div class="camposjuntos">
				<label for="servbanco2">Serv. banco</label>
				<input type="checkbox" name="servbanco2" value="1"
				<?php if($row["serv_banco"] == "1"){
					echo("CHECKED");
				}?>
				DISABLED/>
				<input type="hidden" id="servbanco" name="servbanco" value="<?php echo $row["serv_banco"]; ?>" />
			</div>
			
			<div class="camposjuntos">
				<label for="servalquilercoche2">Serv. alquiler coche</label>
				<input type="checkbox" name="servalquilercoche2" value="1"
				<?php if($row["serv_alquilercoche"] == "1"){
					echo("CHECKED");
				}?>
				DISABLED/>
				<input type="hidden" id="servalquilercoche" name="servalquilercoche" value="<?php echo $row["serv_alquilercoche"]; ?>" />
			</div>
			
			<br /><br />
			<div class="contenedor">
				<br />
			</div>	
			<div>
				<label for="otros">Otros</label>
				<input type="text" class="grande" id="otros" name="otros" value="<?php echo $row["otros"]; ?>"  readonly/>
			</div>
			<br />
			<div>
				<label for="descripcion">Descripcion</label>
				<input type="text" class="grande" id="descripcion" name="descripcion" value="<?php echo $row["descripcion"]; ?>"  readonly/>
			</div>
			<br />

			<div>
				<input type="hidden" id="id" name="id" value="<?php echo $row["id"]; ?>" />
				<input type="submit" class="btn" value="Editar" name="enviar"/>
				<input type="button" class="btn" onClick="javascript:history.back();" value="Cancelar" name="Cancelar"/>
			</div>

		
	</form>
	<form method='POST' id='btnValorar' action = 'valorarPuerto.php'>
				<input type='hidden' id='id' name='id' value="<?php echo $row['id']; ?>" />
				<input type='submit' class='btn' value='Valorar' name='valorar'/>
	</form>
</fieldset>
</body>
</html>