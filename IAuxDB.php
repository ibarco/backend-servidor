<?php
// IAuxDB.php

interface IAuxDB
{
// funciones que se deben implementar obligatoiamente
// en la clase que elija utilizar esta interfaz

	// conecta con la base de datos
	function conectar();
	// desconecta de la base de datos
	function desconectar();
	// ejecuta una instrucción SQL
	function ejecutarSQL($strsql);
	// recupera la siguiente fila de los resultados contenidos en $rst
	function siguienteFila($rst);
	// libera los recursos utilizados
	function liberarRecursos($rst);
}

?>