<?php
include 'AuxDB.php';
include 'clUsuario.php';

$db = new AuxDB();
$db->conectar();

$sql = "SELECT * FROM Usuarios";
$rst = $db->ejecutarSQL($sql);
$NumFilas = $db->cantidadFilas($rst);
$fila = $db->siguienteFila($rst);

$dbtemp = new AuxDB();
$dbtemp->conectar();

$sqltemp = "SELECT * FROM _temp";
$rsttemp = $dbtemp->ejecutarSQL($sqltemp);
$NumFilasTemp = $dbtemp->cantidadFilas($rsttemp);
$filatemp = $dbtemp->siguienteFila($rsttemp);

?>
<html>
<head>
	<title>Tablas _TEMP y USUARIOS</title>
	<link href="css/styles.css" rel="stylesheet" type="text/css" />
	<script src="js/js.js"></script>
</head>
<body>
Usuarios
	<table border='1' id='tabladatosUsuarios' >
		<tr>
			<th>Usuario</th>
			<th>E-mail</th>
			<th>Password</th>
			<th>Perfil</th>
		</tr>

		<?php for ($nf = 1; $nf <= $NumFilas; $nf++) { ?>

			<tr>
				<td class = 'tdUsuarios'><?php printf($fila['Usuario']);?></td>
				<td class = 'tdUsuarios'><?php printf($fila['Mail']);?></td>
				<td class = 'tdUsuarios'><?php printf($fila['Password']);?></td>
				<td class = 'tdUsuarios'><?php printf($fila['Perfil']);?></td>
				<td><button type="button" onClick="javascript:editar('formularioLogin.php');">Ver</button></td>
			</tr>	
			<?php $fila = $db->siguienteFila($rst);
		 } ?>


	</table>
	<br /><br />
	_TEMP
	<table border='1' id='tabladatosUsuarios'>
		<tr>
			<th>Usuario</th>
			<th>e-Mail</th>
			<th>Password</th>
			<th>Text Activacion</th>
		</tr>
		<?php for($nf = 1; $nf <= $NumFilasTemp ; $nf++) { ?>
			<tr>
				<td class = 'tdUsuarios'><?php printf($filatemp['usuario']);?></td>
				<td class = 'tdUsuarios'><?php printf($filatemp['mail']);?></td>
				<td class = 'tdUsuarios'><?php printf($filatemp['password']);?></td>
				<td class = 'tdUsuarios'><?php printf($filatemp['txtActivacion']);?></td>
				<?php echo("<td>" .  "<button type=\"button\" onClick=\"javascript:editar('activar.php?id=" . $filatemp['txtActivacion'] . "');\">Activar</button>");?>
				
			</tr>
			<?php $filatemp = $db->siguienteFila($rsttemp);
		} ?>
	</table>


<?php
	$db->desconectar();
	$dbtemp->desconectar();
?>
<a class = 'tdUsuarios' href='indexUsuarios.php'>Volver</a>
</body>
</html>