<?php
$salida = array();
require "AuxDB.php";
class Valoracion {
	private $valor;
	private $comentario;

	function getValor(){
		return $this->valor;
	}
	function getComentario(){
		return $this->comentario;
	}

	function __Construct($val, $com='') {
		$this->valor = $val;
		$this->comentario = $com;
	}

	function comprobarDatos($user, $puerto) {
		global $salida;
		if (!is_numeric($this->valor)) {
			$salida[] = 'errorN';
		}
		if ($this->valor > '5' || $this->valor < '1') {
			$salida[] = 'errorV';
    	}

    	$db = new AuxDB();
    	$db->conectar();

    	$sql = "SELECT * FROM UsuariosPuertos WHERE idUsuario = '$user' and idPuerto = '$puerto' ";
    	$rst = $db->ejecutarSQL($sql);
    	$fila = $db->siguienteFila($rst);
    	if ($fila) {
    		$salida[] = 'Existe';
    	}
    	$db->desconectar();
   		return $salida;
    }

    function insertar($user, $puerto) {
    	global $salida;
    	$db = new AuxDB();
    	$db->conectar();

    	$v = $this->valor;
	    $c = $this->comentario;
	    $sql = "INSERT INTO UsuariosPuertos VALUES ('$user','$puerto','$v','$c')";
	    $rst = $db->ejecutarSQL($sql);

	    if ($rst) {
	        $salida[] = 'ok';
	    } else {
	    	$salida[] = 'errorI';
	    }
	    $db->desconectar();
	    return $salida;
	    
    }
}