<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html"; charset="utf-8" />
    <title>Formulario para valoración de un Punto de Interés</title>
</head>
<body>
<?php

 include "clValoracion.php";
session_start();
$puntoInteres = $_GET['idPunto'];
// Aun no hemos rellenado el formulario
if (!isset($_POST['enviar'])) {?>

    <div>
        <form method="POST" action="JSON/valorarPuntoInteres.php">
            <fieldset>
                <legend>Valorar <?php printf($puntoInteres);?>:</legend>   
            	   <div>
                		<label for="valorarion">Valoración:(1-5)</label>
                		<input type='text' name='valoracion'/>
            	   </div>
                </br>
            	   <div>
            		  <label for="comentario">Comentario:</label>
            		  <input type='text' name='comentario'/>
            	   </div>
                </br>
                      <input type='hidden' name='idPunto' value='<?php printf($puntoInteres); ?>' />
            	      <input type="submit" class="btn" value="Login" name="enviar"/>
            </fieldset>
        </form>
    </div>
<?php   
} else {
   
    $user = $_SESSION['userid'];
    $valorar = new Valoracion($_POST['valoracion'],$_POST['comentario']);
    $punto = $_POST['idPunto'];
    //$valor = $_POST['valoracion'];
    //$come = $_POST['comentario'];

    $salida = $valorar->comprobarDatos($user,$punto);
    
    if ($salida == 'ok') {
        $salida = $valorar->insertar($user,$punto);
    }
 
    echo json_encode($salida);

    ?>
    <!--<script language='javascript'>
                window.location = "./tablaPuntosInteres.php";
    </script>-->
<?php
}?>

</body>

</html>
