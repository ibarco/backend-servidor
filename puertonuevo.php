<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Puerto</title>
	<link href="css/styles.css" rel="stylesheet" type="text/css" />
	<script src="js/js.js"></script>
</head>
<body>
	<div class="titulo"><a class="titulo" href="index.html">iBarco</a></div>
	<br><br>
	<form id="formPuerto" method="POST" action="puertoinserta.php">
		<fieldset>
			<legend class="titulo">Nuevo Puerto</legend>   
			<div>
				<label for="nombre">Nombre</label>
				<input type="text" class="grande" id="nombre" name="nombre"/>
			</div>
			<br />
			<div>
				<label for="latitud">Latitud</label>
				<input type="text" class="corto" id="latitud" name="latitud"/>
			</div>
			<br />
			<div>
				<label for="longitud">Longitud</label>
				<input type="text" class="corto" id="longitud" name="longitud"/>
			</div>
			<br />
			<div>
				<label for="direccion">Direccion</label>
				<input type="text" class="corto" id="direccion" name="direccion"/>
			</div>
			<br />
			<div>
				<label for="poblacion">Poblacion</label>
				<input type="text" class="corto" id="poblacion" name="poblacion"/>
			</div>
			<br />
			<div>
				<label for="cp">CP</label>
				<input type="text" class="corto" id="cp" name="cp"/>
			</div>
			<br />
			<div>
				<label for="provincia">Provincia</label>
				<input type="text" class="corto" id="provincia" name="provincia"/>
			</div>
			<br />
			<div>
				<label for="telefono">Telefono</label>
				<input type="text" class="corto" id="telefono" name="telefono"/>
			</div>
			<br />
			<div>
				<label for="fax">Fax</label>
				<input type="text" class="corto" id="fax" name="fax"/>
			</div>
			<br />
			<div>
				<label for="www">Www</label>
				<input type="text" class="corto" id="www" name="www"/>
			</div>
			<br />
			<div>
				<label for="email">Email</label>
				<input type="text" class="corto" id="email" name="email"/>
			</div>
			<br />
			<div>
				<label for="caladomin">Calado minimo</label>
				<input type="text" class="corto" id="caladomin" name="caladomin"/>
			</div>
			<br />
			<div>
				<label for="esloramax">Eslora maxima</label>
				<input type="text" class="corto" id="esloramax" name="esloramax"/>
			</div>
			<br />
			<div>
				<label for="namarres">N de amarres</label>
				<input type="text" class="corto" id="namarres" name="namarres"/>
			</div>
			<br />
			<div>
				<label for="canalvhf">Canal vhf</label>
				<input type="text" class="corto" id="canalvhf" name="canalvhf"/>
			</div>
			<br />
			<div class="camposjuntos">
				<label for="servagua">Serv. Agua</label>
				<input type="checkbox" name="servagua" value="1" />
			</div>
			<div class="camposjuntos">
				<label for="servelectricidad">Serv. Electricidad</label>
				<!--<input type="text" class="corto" id="servelectricidad" name="servelectricidad"/>-->
				<input type="checkbox" name="servelectricidad" value="1" />
			</div>
			<div class="camposjuntos">
				<label for="servgasolina">Serv. Gasolina</label>
				<!--<input type="text" class="corto" id="servgasolina" name="servgasolina"/>-->
				<input type="checkbox" name="servgasolina" value="1" />
			</div>
			<div class="camposjuntos">
				<label for="servgrua">Serv. Grua</label>
				<!--<input type="text" class="corto" id="servgrua" name="servgrua"/>-->
				<input type="checkbox" name="servgrua" value="1" />
			</div>
			<div class="camposjuntos">
				<label for="servtravellift">Serv. travellift</label>
				<!--<input type="text" class="corto" id="servtravellift" name="servtravellift"/>-->
				<input type="checkbox" name="servtravellift" value="1" />
			</div>
			<div class="camposjuntos">
				<label for="servrampa">Serv. rampa</label>
				<!--<input type="text" class="corto" id="servrampa" name="servrampa"/>-->
				<input type="checkbox" name="servrampa" value="1" />
			</div>
			<div class="camposjuntos">
				<label for="servtaller">Serv. taller</label>
				<!--<input type="text" class="corto" id="servtaller" name="servtaller"/>-->
				<input type="checkbox" name="servtaller" value="1" />
			</div>
			<div class="camposjuntos">
				<label for="servmuelleespera">Serv. muelle de espera</label>
				<!--<input type="text" class="corto" id="servmuelleespera" name="servmuelleespera"/>-->
				<input type="checkbox" name="servmuelleespera" value="1" />
			</div>
			<div class="camposjuntos">
				<label for="servbar">Serv. Bar</label>
				<!--<input type="text" class="corto" id="servbar" name="servbar"/>-->
				<input type="checkbox" name="servbar" value="1" />
			</div>
			<div class="camposjuntos">
				<label for="servrestaurante">Serv. restaurante</label>
				<!--<input type="text" class="corto" id="servrestaurante" name="servrestaurante"/>-->
				<input type="checkbox" name="servrestaurante" value="1" />
			</div>
			<div class="camposjuntos">
				<label for="servsupermercado">Serv. supermercado</label>
				<!--<input type="text" class="corto" id="servsupermercado" name="servsupermercado"/>-->
				<input type="checkbox" name="servsupermercado" value="1" />
			</div>
			<div class="camposjuntos">
				<label for="servhielo">Serv. hielo</label>
				<!--<input type="text" class="corto" id="servhielo" name="servhielo"/>-->
				<input type="checkbox" name="servhielo" value="1" />
			</div>
			<div class="camposjuntos">
				<label for="servrecogidaaceite">Serv. recogida aceite</label>
				<!--<input type="text" class="corto" id="servrecogidaaceite" name="servrecogidaaceite"/>-->
				<input type="checkbox" name="servrecogidaaceite" value="1" />
			</div>
			<div class="camposjuntos">
				<label for="servrecogidabasura">Serv. recogida basura</label>
				<!--<input type="text" class="corto" id="servrecogidabasura" name="servrecogidabasura"/>-->
				<input type="checkbox" name="servrecogidabasura" value="1" />
			</div>
			<div class="camposjuntos">
				<label for="servaguasnegras">Serv. aguas negras</label>
				<!--<input type="text" class="corto" id="servaguasnegras" name="servaguasnegras"/>-->
				<input type="checkbox" name="servaguasnegras" value="1" />
			</div>
			<div class="camposjuntos">
				<label for="servfarmacia">Serv. farmacia</label>
				<!--<input type="text" class="corto" id="servfarmacia" name="servfarmacia"/>-->
				<input type="checkbox" name="servfarmacia" value="1" />
			</div>
			<div class="camposjuntos">
				<label for="servmedico">Serv. medico</label>
				<!--<input type="text" class="corto" id="servmedico" name="servmedico"/>-->
				<input type="checkbox" name="servmedico" value="1" />
			</div>
			<div class="camposjuntos">
				<label for="servmeteo">Serv. meteo</label>
				<!--<input type="text" class="corto" id="servmeteo" name="servmeteo"/>-->
				<input type="checkbox" name="servmeteo" value="1" />
			</div>
			<div class="camposjuntos">
				<label for="servbanco">Serv. banco</label>
				<!--<input type="text" class="corto" id="servbanco" name="servbanco"/>-->
				<input type="checkbox" name="servbanco" value="1" />
			</div>
			<div class="camposjuntos">
				<label for="servalquilercoche">Serv. alquiler coche</label>
				<!--<input type="text" class="corto" id="servalquilercoche" name="servalquilercoche"/>-->
				<input type="checkbox" name="servalquilercoche" value="1" />
			</div>
			<br /><br />
			<div class="contenedor">
				<br />
			</div>	
			<div>
				<label for="otros">Otros</label>
				<input type="text" class="grande" id="otros" name="otros"/>
			</div>
			<br />
			<div>
				<label for="descripcion">Descripcion</label>
				<input type="text" class="grande" id="descripcion" name="descripcion"/>
			</div>
			<br />
			<div>
				<input type="submit" class="btn" value="Guardar" name="enviar"/>
				<input type="button" class="btn" onClick="javascript:history.back();" value="Cancelar" name="Cancelar"/>
			</div>
		</fieldset>
	</form>
</body>
</html>