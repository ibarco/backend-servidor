<?php
include "AuxDB.php";
require("funciones.php");

$idruta= getParam($_GET["id"], "-1");

$sql = "SELECT * FROM Rutas WHERE id = ".sqlValue($idruta, "int");
//Establecemos conexión con la BD
$db = new AuxDB();
$db->conectar();
//Ejecutamos la consulta SQL
$result = $db->ejecutarSQL($sql);
$row = $db->siguienteFila($result);

$total = $db->cantidadFilas($result);
if ($total == 0) {
	header("location: rutas.php");
	exit;
}
?>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Puerto</title>
	<link href="css/styles.css" rel="stylesheet" type="text/css" />
	<script src="js/js.js"></script>
</head>
<body>
	<div class="titulo"><a class="titulo" href="index.html">iBarco</a></div>
	<br><br>
	<form method="post" id="formRuta" action="rutaedita.php">	

		<fieldset>
			<legend class="titulo">Visualizando datos de Ruta</legend>   
			<div>
				<label for="nombre">Nombre</label>
				<input type="text" class="grande" id="nombre" name="nombre" value="<?php echo $row["nombre"]; ?>" readonly/>
			</div>
			<br />
			<div>
				<label for="latitud">Descripcion</label>
				<input type="text" class="grande" id="descripcion" name="descripcion" value="<?php echo $row["descripcion"]; ?>"  readonly/>
			</div>
			<br />
			<div>
				<label for="longitud">Distancia</label>
				<input type="text" class="corto" id="distancia" name="distancia" value="<?php echo $row["distancia"]; ?>"  readonly/>
			</div>
			<br />
			<div>
				<input type="hidden" id="id" name="id" value="<?php echo $row["id"]; ?>" />
				<input type="submit" class="btn" value="Editar" name="enviar"/>
				<input type="button" class="btn" onClick="javascript:history.back();" value="Cancelar" name="Cancelar"/>
			</div>	
	</form>
			<form method='POST' id='btnValorar' action = 'valorarRutas.php'>
				<input type='hidden' id='id' name='id' value="<?php echo $row['id']; ?>" />
				<input type='submit' class='btn' value='Valorar' name='valorar'/>
			</form>
			
		</fieldset>
	
</body>
</html>