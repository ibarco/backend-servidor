<?php

require_once "AuxDB.php";

// clase Ruta
class Ruta {
	
	private $ruta_ID;
	private $ruta_Nombre;
	private $ruta_Descripcion;
	private $ruta_Distancia;

	// Funciones de la clase Ruta
	function getRutaID() {
		return $this->ruta_ID;
	}
	function getRutaNombre(){
		return $this->ruta_Nombre;
	}
	function getRutaDescripcion() {
		return $this->ruta_Descripcion;
	}
	function getRutaDistancia() {
		return $this->ruta_Distancia;
	}



	//Constructor objeto Ruta
	function __construct($rID, $rNombre, $rDescripcion, $rDistancia){
		$this->ruta_ID = $rID;
		$this->ruta_Nombre = $rNombre;
		$this->ruta_Descripcion = $rDescripcion;
		$this->ruta_Distancia = $rDistancia;
	}


	function leerRutaNombre($rNombre) {
		$db = new AuxDB();
		$db->conectar();
		$sql = "SELECT * FROM Rutas WHERE nombre = '" . $rNombre . "'";

		$rst = $db->ejecutarSQL($sql);
		$db->desconectar();

		$fila = $db->siguienteFila($rst);
		
		$this-> $ruta_Nombre = $rNombre;
		$this-> $ruta_Id = $fila['id'];
		$this-> $ruta_Descripcion = $fila['descripcion'];
		$this-> $ruta_Distancia = $fila['distancia'];
		$db->desconectar();
	}

	function leerRutaID($rID) {
		$db = new AuxDB();
		$db->conectar();
		$sql = "SELECT * FROM Rutas WHERE id = '" . $rID . "'";

		$rst = $db->ejecutarSQL($sql);
		$db->desconectar();

		$fila = $db->siguienteFila($rst);
		
		$this->$ruta_Id = $rID;
		$this->$ruta_Nombre = $fila['nombre'];
		$this->$ruta_Descripcion = $fila['descripcion'];
		$this->$ruta_Distancia = $fila['distancia'];
		$db->desconectar();
	}

	// Inserta nueva Ruta
	function insertarRuta() {

		$db=new AuxDB();
		$db->conectar();
		$sql = "INSERT INTO Rutas(nombre, descripcion, distancia) ";
		$sql.= "VALUES ('". mysql_escape_string($this->ruta_Nombre) . "', '". mysql_escape_string($this->ruta_Descripcion) . "', '".mysql_escape_string($this->ruta_Distancia). "')";

		$db->ejecutarSQL($sql);
		//$ret=0;
		//if ($db) { //insercion correcta
			//$ret=1;
		//}
		$db->desconectar();
		return $ret;
	} 

	//Modificar Ruta
	function modificaRuta() {
		$db = new AuxDB();
		$db->conectar();

		$rID = $this->ruta_ID;
		$nombreNuevo = $this->ruta_Nombre;
		$descripcionNuevo = $this->ruta_Descripcion;
		$distanciaNuevo = $this->ruta_Distancia;

		$sql = "UPDATE Rutas SET ";
		$sql.= "nombre='".$nombreNuevo."', descripcion='".$descripcionNuevo."', distancia='".$distanciaNuevo . "' ";
		$sql.= "WHERE id=$rID";

		$rst = $db->ejecutarSQL($sql);

		if ($rst) {
			return 'ok';
		} else {
			return 'ko';
		}
	}

	function eliminarRuta() {
		$db = new AuxDB();
		$db->conectar();

		$rID = $this->ruta_ID;
		$nombreNuevo = $this->ruta_Nombre;
		$descripcionNuevo = $this->ruta_Descripcion;
		$distanciaNuevo = $this->ruta_Distancia;

		$sqlexiste = "SELECT * FROM Rutas WHERE id = '$rID'";
		$rstexiste = $db->ejecutarSQL($sqlexiste);
		$fila = $db->siguienteFila($rstexiste);

		if ($fila) {

			$sql = "DELETE FROM Rutas WHERE id = '$rID'";
			$rst = $db->ejecutarSQL($sql);
			return 'Borrado';
		} else {
			return 'No';
		}
		$db->desconectar();
	}

}

?>