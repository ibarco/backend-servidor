<?php

require_once "AuxDB.php";
$url = '';
$clave = '';
// clase Usuario

class Usuario {
	
	private $UsuarioID;
	private $Email;
	private $Password;
	
	
	// Funciones de la clase Usuario
	function getUsuarioID() {
		return $this->UsuarioID;
	}
	function getEmail(){
		return $this->Email;
	}
	function getPassword() {
		return $this->Password;
	}
	
	function __Construct($Usu, $Ema, $Pas){
		
	 // Es el contructor del objeto Usuario
	$this->UsuarioID = $Usu;
	$this->Email = $Ema;
	$this->Password=$Pas;
	}

	function generar_txtAct($longitud,$especiales){ 
		global $clave;
		// Array con los valores a escojer
        $semilla = array(); 
        $semilla[] = array('a','e','i','o','u');  
        $semilla[] = array('b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','y','z'); 
        $semilla[] = array('0','1','2','3','4','5','6','7','8','9'); 
        $semilla[] = array('A','E','I','O','U');  
        $semilla[] = array('B','C','D','F','G','H','J','K','L','M','N','P','Q','R','S','T','V','W','X','Y','Z'); 
        $semilla[] = array('0','1','2','3','4','5','6','7','8','9'); 
        // si puede contener caracteres especiales, aumentamos el array $semilla 
        if ($especiales) { $semilla[] = array('$','#','%','&','@','-','?','¿','!','¡','+','-','*'); } 
        // creamos la clave con la longitud indicada 
			    for ($bucle=0; $bucle < $longitud; $bucle++)  
			    { 
			        // seleccionamos un subarray al azar 
			        $valor = mt_rand(0, count($semilla)-1); 
			        // selecccionamos una posicion al azar dentro del subarray 
			        $posicion = mt_rand(0,count($semilla[$valor])-1); 
			        // cojemos el caracter y lo agregamos a la clave 
			        $clave .= $semilla[$valor][$posicion]; 
			        } 
		// devolvemos la clave 
		return $clave; 
	}

	function leerUsuario() {
		$db = new AuxDB();
		$db->conectar();
		$sql = "SELECT * FROM Usuarios WHERE Usuario = '" . $this->UsuarioID . "'";
		
		$rst = $db->ejecutarSQL($sql);
		
		$fila = $db->siguienteFila($rst);
		$this->Email = $fila['Mail'];
		$this->Password = $fila['Password'];
		$db->desconectar();
		
	}
	
	// valida el Usuario.
	// correcto : retorna el nombre del usuario
	// formato falla : retorna 'error'
	// existe : retorna 'existe'
	function validarUsuario() {
		// miramos si el formato de $usuario es el correcto
		$permitidos = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_ ";
		$caracterKO = 0;
		if(strlen($this->UsuarioID) < 5){
			return 'error';
		}
		else {
			for ($i=0; $i<strlen($this->UsuarioID); $i++){
			    if (strpos($permitidos, substr($this->UsuarioID,$i,1))===false) {
	 		   	  $caracterKO = 1;
	 		   	}
			}
		}
		if ($caracterKO == 1 || strlen($this->UsuarioID) <= 4){
			return 'error';
		}
		// formato correcto
		// miramos si existe en la tabla Usuarios
		$db = new AuxDB();
		$db->conectar();

		$sql = "SELECT Usuario  from Usuarios where Usuario = '$this->UsuarioID'";
		
		$res = $db->ejecutarSQL($sql);
		
		$fila = $db->siguienteFila($res);

		$db->desconectar();
		if ($fila) {
			return 'existe';
		}
		else {
			return $this->UsuarioID;
		}
	}

	// Valida el Password del usuario
	// Correcto 		  : retorna el password del usuario
	// Falla longitud     : retorna error
	// Falla caracteres   : retorna carac
	// Falla pass1!=pass2 : retorna dif
	function validarPassword($pass) {
		//NO tiene minimo de 5 caracteres o mas de 12 caracteres
		if(strlen($pass) < 5 || strlen($pass) > 12)
			return 'long';
		// SI longitud, NO VALIDO numeros y letras
		else if(!preg_match("/^[0-9a-zA-Z]+$/", $pass))
			return 'carac';
		// SI rellenado, SI password valido
			//NO coinciden
		if($pass != $this->Password)
			return 'dif';
		else
			return $this->Password;

	}


	// Valida el Email del usuario
	// Correcto 		  : retorna el email del usuario
	// Falla longitud     : retorna errorE
	// Existe             : retorna existeE
	// Diferentes         : retorna difE
	function validarEmail($email){
		$mail_correcto = 0;
		if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){ 
  	   		 if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) { 
         		 //miro si tiene caracter . 
         		 if (substr_count($email,".")>= 1){ 
            		 //obtengo la terminacion del dominio 
            		 $term_dom = substr(strrchr ($email, '.'),1); 
            		 //compruebo que la terminación del dominio sea correcta 
            		 if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){ 
            	   	 //compruebo que lo de antes del dominio sea correcto 
            	   		 $antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1); 
            	   		 $caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1); 
            	   		 if ($caracter_ult != "@" && $caracter_ult != "."){ 
            	   	   	 	$mail_correcto = 1; 
            	   		 } 
            		 } 
         		 } 
      		 } 
	   	} 
  	 	if (!$mail_correcto) {
      		 return 'errorE'; 
		}
      	// miro que ya no exista el mail en la BDD
      	$db = new AuxDB();
      	$db->conectar();
		$sql = "SELECT Usuario FROM Usuarios  WHERE Mail = '$this->Email'";
		$rst = $db->ejecutarSQL($sql);

		if ($db->cantidadFilas($rst)>0){
			return 'existeE';
		} else {
			$sql1 = "SELECT * FROM _temp where mail = '$this->Email'";
			$rst1 = $db->ejecutarSQL($sql1);
			if ($db->cantidadFilas($rst1)>0){
				return 'existeE';
			} 
			else {
				if($email == $this->Email) {
					return $this->Email;
				} 
				else {
					return 'difE';
				}
			}
			
		}
	}


	// Inserta el usuario en la tabla _temp a la espera de ser validado
	function insertarUsuarioTemp() {
		global $usb;
		global $clave;

		$db=new AuxDB();
		$db->conectar();

		$clave = $this->generar_txtAct(20,false);
		$url = "activar.php?id=" . $clave;
		
		// $sql="INSERT INTO _temp(usuario, mail, password,fechaAlta,txtActivacion)";
		// $sql.=" VALUES ('" . mysql_escape_string($this->UsuarioID) . "', '";
		// $sql.= mysql_escape_string($this->Email) . "', '";
		// $sql.= md5(mysql_escape_string($this->Password)) ."','";
		// $sql.= "CURDATE(),";
		// $sql.= mysql_escape_string($clave)."')";
		$sql = "INSERT INTO _temp(usuario, mail, password,fechaAlta,txtActivacion) ";
		$sql.= "VALUES ('".mysql_escape_string($this->UsuarioID)."', '".mysql_escape_string($this->Email)."', '".md5(mysql_escape_string($this->Password))."',";
		$sql.="	CURDATE(),'".mysql_escape_string($clave)."')";
		$db->ejecutarSQL($sql);
		$ret=0;
		if ($db) { //insercion correcta
			$ret=1;
		}
		$db->desconectar();
		return $ret;
	} 

	// Envia un mail al usuario para activar definitivamente su cuenta
	function mailActivacion() {
		global $url;
		global $clave;

		$url = "activar.php?id=" . $clave;
		$dominio = "http://www.marinaferry.es/sandbox/iBarco/";
		$destinatario = $this->getEmail(); 
		$asunto = "iBarco. Activación de usuario"; 
		$cuerpo = ' 
				<html> 
					<head> 
	   				<title>iBarco - Activar usuario</title> 
					</head> 
					<body> 
						Hola ';
		$cuerpo .= $this->getUsuarioID;
		$cuerpo .= '<p>Gracias por adquirir y registrarte en la aplicación <b>iBarco</b>.</p>
				<p>Para completar el registro tienes que confirmar que has recibido el e-mail en el siguiente enlace:</p>
				<p style=text-align:center><a href=';
		$cuerpo .= $dominio . $url;
		$cuerpo .= " target=_blank>Activa tu usuario</a></p></body></html>";
		$cuerpo .= "<br><br><br>";
		$cuerpo .= "Si tu correo no te permite ejecutarlo, copia y pega en tu navegador la siguiente dirección:<br>";
		$cuerpo .= $dominio . $url;
		$cuerpo .= "</body></html>";
		//para el envío en formato HTML 
		$headers = "MIME-Version: 1.0\r\n"; 
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
		//dirección del remitente 
		$headers .= "From: iBarco <nombre@ibarco.com>\r\n";  //Hay que configurar un usario de correo
		//dirección de respuesta, si queremos que sea distinta que la del remitente 
		$headers .= "Reply-To: nombre@ibarco.com\r\n"; 
		//ruta del mensaje desde origen a destino 
		//$headers .= "Return-path: holahola@desarrolloweb.com\r\n"; 	
		//direcciones que recibián copia 
		//$headers .= "Cc: otra@ibarco.com\r\n"; 
		//direcciones que recibirán copia oculta 
		//$headers .= "Bcc: otra@ibarco.com\r\n"; 
		//En localhost el envío de e-mail a veces no funciona, hay que configurar algunas cosas.

		$bool = mail($destinatario,$asunto,$cuerpo,$headers);
	}

	function login() {

		$db = new AuxDB;
		$db->conectar();

		$u = $this->UsuarioID;
		$p = $this->Password;

		$sql = "SELECT * FROM Usuarios WHERE Usuario = '$u' and Password = '".md5($p)."'";
		$rst = $db->ejecutarSQL($sql);

		$filas = $db->cantidadFilas($rst);

		if ($filas > 0) {
			return('registrado');
		}else {
			return('No');
		}
		$db->desconectar();
	}

	function baja() {
		$db = new AuxDB();
		$db->conectar();

		$u = $this->UsuarioID;
		$p = $this->Password;

		$sqlexiste = "SELECT * FROM Usuarios WHERE Usuario = '$u' and Password = '".md5($p)."'";
		$rstexiste = $db->ejecutarSQL($sqlexiste);
		$fila = $db->siguienteFila($rstexiste);

		if ($fila) {

			$sql = "DELETE FROM Usuarios WHERE Usuario = '$u' and Password = '".md5($p)."'";
			$rst = $db->ejecutarSQL($sql);
			return 'Borrado';
		} else {
			return 'No';
		}
		$db->desconectar();
	}

	function cambiarPassword($pass) {
		$db = new AuxDB();
		$db->conectar();

		$passAntiguo = $this->Password;

		$sql = "UPDATE Usuarios SET Password = '".md5($pass)."' WHERE Password = '$passAntiguo'";
		$rst = $db->ejecutarSQL($sql);

		if ($rst) {
			return 'cambiado';
		} else {
			return 'errorCambio';
		}
	}

}
?>