<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html"; charset="utf-8" />
    <title>Formulario de registro de los Puntos de Interés</title>
    <link href="css/styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="titulo"><a class="titulo" href="index.html">iBarco Puntos de interés</a></div>
<br><br>
    <form method="POST" action="insertarPunto.php">
        <fieldset>
            <legend class='titulo'>Ingresar datos del Punto de Interés</legend>   
        	   <div>
            		<label for="nombre" >Nombre:</label>
            		<input type='text'  name='nombre'/>
        	   </div>
            </br>
        	   <div>
                  <label for="longitud" class='grande'>Longitud:</label>
                  <input type="text"  name="longitud" />
        	   </div>
            </br>
                <div>
                    <label for="latitud" class='grande'>Latitud: </label>
                    <input type="text"  name="latitud"/>
                </div>
            </br>
                <div>
                    <label for="descripcion">Descripción: </label>
                    <input type="text" class='grande' name="descripcion"/>
                <div>
            </br>
                <div>
                    <label for="fotos">Ubicación Fotos: </label>
                    <input type="text" class='grande' name="fotos"/>
                <div>
            </br>
        	   <input type="submit" class="btn" value="Entrar" name="enviar"/>
        </fieldset>
    </form>
</div>
</body>
</html>