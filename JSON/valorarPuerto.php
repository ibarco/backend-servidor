<?php
// PArametros a pasar:
//            id : identificador del puerto. Se le pasa por Post
//            $_SESSION : datos del usuario identificado
//            valoracion y comentario: son los datos de la valoracion. Por POST
// Salida:
//          ErrorN: valoracion no numerica
//          ErrorV: valoracion entre 1-5
//          Existe: valoracion ya efectuada
//          ok : correcto

include "../clValoracionPuerto.php";
session_start();

$salida = array();
   
$user = $_SESSION['userid'];
$valorar = new Valoracion($_POST['valoracion'],$_POST['comentario']);
$puerto = $_POST['id'];

$salida = $valorar->comprobarDatos($user,$puerto);
    
if (count($salida) == 0) {
    $salida = $valorar->insertar($user,$puerto);
}
 
echo json_encode($salida);
?>