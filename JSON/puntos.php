<?php
include "../AuxDB.php";


//Establecemos conexión con la BD
$db = new AuxDB();
$db->conectar();
//Ejecutamos la consulta SQL
$sql = "SELECT * FROM Puntos ORDER BY Nombre";
$result = $db->ejecutarSQL($sql);


$array_puntos = array();

//Recorremos las filas de la consulta
while($row = $db->siguienteFila($result) ){	
	$array_puntos[] = array (
		'id'=> $row["id"],
		'nombre' => htmlentities($row["Nombre"]),
		'longitud' => $row["Longitud"], 	
		'latitud' => $row["Latitud"], 
		'descripcion' => htmlentities($row["Descripcion"]),
		'fotos' => htmlentities($row["Fotos"])
	 );
}
echo json_encode($array_puntos);

?>