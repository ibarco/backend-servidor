<?php
include "AuxDB.php";

	//Establecemos conexión con la BD
	$db = new AuxDB();
	$db->conectar();
	//Ejecutamos la consulta SQL
	$sql = "SELECT * FROM Puertos";
	$result = $db->ejecutarSQL($sql);


	//Creamos el array  de valores
	$puertos = array();
	$respuesta = array();

	while($row = $db->siguienteFila($result) ){	
		$nombre = $row["nombre"];
		$poblacion = $row["dir_poblacion"];
		$longitud = $row["longitud"];

		$puertos[] = array("Nombre"=>$nombre, "Poblacion"=>$poblacion, "Longitud"=>$longitud);

	}

	$respuesta["puertos"] = $puertos;

	echo json_encode($respuesta);







?>
