<?php
// PArametros a pasar:
//            id : identificador de la ruta. Se le pasa por Get
//            $_SESSION : datos del usuario identificado
//            valoracion y comentario: son los datos de la valoracion. Por POST
// Salida:
//          ErrorN: valoracion no numerica
//          ErrorV: valoracion entre 1-5
//          Existe: valoracion ya efectuada
//          ok : correcto

include "../clValoracionRuta.php";
session_start();

$salida = array();

$ruta = $_GET['id'];
   
$user = $_SESSION['userid'];
$valorar = new Valoracion($_POST['valoracion'],$_POST['comentario']);
$ruta = $_POST['id'];

$salida = $valorar->comprobarDatos($user,$ruta);
    
if (count($salida) == 0) {
    $salida = $valorar->insertar($user,$ruta);
}
 
echo json_encode($salida);
?>