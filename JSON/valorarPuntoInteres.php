<?php
// PArametros a pasar:
//            idPunto : identificador del punto. Se le pasa por Get
//            $_SESSION : datos del usuario identificado
//            valoracion y comentario: son los datos de la valoracion. Por POST
// Salida:
//          ErrorN: valoracion no numerica
//          ErrorV: valoracion entre 1-5
//          Existe: valoracion ya efectuada
//          ok : correcto

include "../clValoracionPunto.php";
session_start();

$salida = array();

$puntoInteres = $_GET['idPunto'];
   
$user = $_SESSION['userid'];
$valorar = new Valoracion($_POST['valoracion'],$_POST['comentario']);
$punto = $_POST['idPunto'];

$salida = $valorar->comprobarDatos($user,$punto);
    
if (count($salida) == 0) {
    $salida = $valorar->insertar($user,$punto);
}
 
echo json_encode($salida);
?>
  
