<?php
include "../AuxDB.php";

if (isset($_GET['usernombre'])) {
//if (isset($_POST['usernombre'])) {
	$usuario = $GET['usernombre'];
//$usuario = $_POST['usernombre'];

//Establecemos conexión con la BD
	$db = new AuxDB();
	$db->conectar();
//Ejecutamos la consulta SQL
	$sql = "SELECT * FROM Rutas Where autor=" . $usuario;
	$result = $db->ejecutarSQL($sql);

	$array_rutas = array();

//Recorremos las filas de la consulta
	while($row = $db->siguienteFila($result) ){	
		$array_rutas[] = array (
			'id'=> $row["id"],
			'nombre' => htmlentities($row["nombre"]),
			'descripcion' => htmlentities($row["descripcion"]), 	
			'distancia' => htmlentities($row["distancia"])
			);
	}
	echo json_encode($array_rutas);

} else  {
	$array_rutas[] = array ('id' => 'error');
	echo json_encode($array_rutas);
}


?>