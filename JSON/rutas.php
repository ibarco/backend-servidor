<?php
include "../AuxDB.php";


//Establecemos conexión con la BD
$db = new AuxDB();
$db->conectar();
//Ejecutamos la consulta SQL
$sql = "SELECT * FROM Rutas";
$result = $db->ejecutarSQL($sql);


$array_puertos = array();

//Recorremos las filas de la consulta
while($row = $db->siguienteFila($result) ){	
	$array_puertos[] = array (
		'id'=> $row["id"],
		'nombre' => htmlentities($row["nombre"]),
		'descripcion' => $row["descripcion"], 	
		'distancia' => htmlentities($row["distancia"])
	 );
}
echo json_encode($array_puertos);

?>