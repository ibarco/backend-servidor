<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Puerto</title>
	<link href="css/styles.css" rel="stylesheet" type="text/css" />
	<script src="js/js.js"></script>
</head>
<body>
	<div class="titulo"><a class="titulo" href="index.html">iBarco</a></div>
	<br><br>
	<form method="POST" action="rutainserta.php">
		<fieldset>
			<legend class="titulo">Nueva Ruta</legend>   
			<div>
				<label for="nombre">Nombre</label>
				<input type="text" class="grande" id="nombre" name="nombre"/>
			</div>
			<br />
			<div>
				<label for="descripcion">Descripcion</label>
				<input type="text" class="grande" id="descripcion" name="descripcion"/>
			</div>
			<br />
			<div>
				<label for="distancia">Distancia</label>
				<input type="text" class="corto" id="distancia" name="distancia"/>
			</div>
			<br />
			<div>
				<input type="submit" class="btn" value="Guardar" name="enviar"/>
				<input type="button" class="btn" onClick="javascript:history.back();" value="Cancelar" name="Cancelar"/>
			</div>
		</fieldset>
	</form>
</body>
</html>