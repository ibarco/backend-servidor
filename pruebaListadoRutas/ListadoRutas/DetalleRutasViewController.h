//
//  DetalleRutasViewController.h
//  ListadoRutas
//
//  Created by chus on 30/11/12.
//  Copyright (c) 2012 chus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ruta.h"

@interface DetalleRutasViewController : UIViewController

@property (nonatomic,strong) Ruta *ruta;

@property (strong, nonatomic) IBOutlet UILabel *nombreLabel;
@property (strong, nonatomic) IBOutlet UILabel *descripcionLabel;
@property (strong, nonatomic) IBOutlet UILabel *distanciaLabel;


@end
