//
//  Rutas.h
//  ListadoRutas
//
//  Created by chus on 07/12/12.
//  Copyright (c) 2012 chus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Rutas : NSObject

@property (nonatomic, strong) NSMutableArray *idRuta;
@property (nonatomic, strong) NSMutableArray *nombre;
@property (nonatomic, strong) NSMutableArray *descripcion;
@property (nonatomic, strong) NSMutableArray *distancia;

@end
