//
//  Conexion.h
//  ListadoRutas
//
//  Created by chus on 07/12/12.
//  Copyright (c) 2012 chus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Conexion : NSObject {
    NSURLConnection *conexion;
}

    -(NSDictionary *) datos;

@end
