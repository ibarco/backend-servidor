//
//  DetalleRutasViewController.m
//  ListadoRutas
//
//  Created by chus on 30/11/12.
//  Copyright (c) 2012 chus. All rights reserved.
//

#import "DetalleRutasViewController.h"

@interface DetalleRutasViewController ()

@end

@implementation DetalleRutasViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.nombreLabel.text = self.ruta.nombre;
    self.descripcionLabel.text = self.ruta.descripcion;
    self.distanciaLabel.text = self.ruta.distancia;
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
