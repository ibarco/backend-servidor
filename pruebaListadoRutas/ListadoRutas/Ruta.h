//
//  Ruta.h
//  ListadoRutas
//
//  Created by chus on 07/12/12.
//  Copyright (c) 2012 chus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Ruta : NSObject

@property (nonatomic, strong) NSString *idRuta;
@property (nonatomic, strong) NSString *nombre;
@property (nonatomic, strong) NSString *descripcion;
@property (nonatomic, strong) NSString *distancia;

@end
