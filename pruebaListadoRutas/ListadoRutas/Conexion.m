//
//  Conexion.m
//  ListadoRutas
//
//  Created by chus on 07/12/12.
//  Copyright (c) 2012 chus. All rights reserved.
//

#import "Conexion.h"

@implementation Conexion

-(NSDictionary *) datos {
    NSData * data=[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://www.marinaferry.es/sandbox/iBarco/JSON/rutas.php"]];
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    return dic;
}
@end
